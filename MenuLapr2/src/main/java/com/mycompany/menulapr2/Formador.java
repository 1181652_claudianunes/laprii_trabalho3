/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.menulapr2;
/**
 *
 * @author Cláudia Nunes
 */
public class Formador {
    
    private int nif;
    private String nomeCompleto;
    private String rua;
    private String codPostal;
    private int numTelefone;

    public Formador(int nif, String nomeCompleto, String rua, String codPostal, int numTelefone) {
        this.nif = nif;
        this.nomeCompleto = nomeCompleto;
        this.rua = rua;
        this.codPostal = codPostal;
        this.numTelefone = numTelefone;
    }

    public int getNif() {
        return nif;
    }

    public void setNif(int nif) {
        this.nif = nif;
    }

    public String getNomeCompleto() {
        return nomeCompleto;
    }

    public void setNomeCompleto(String nomeCompleto) {
        this.nomeCompleto = nomeCompleto;
    }

    public String getRua() {
        return rua;
    }

    public void setRua(String rua) {
        this.rua = rua;
    }

    public String getCodPostal() {
        return codPostal;
    }

    public void setCodPostal(String codPostal) {
        this.codPostal = codPostal;
    }

    public int getNumTelefone() {
        return numTelefone;
    }

    public void setNumTelefone(int numTelefone) {
        this.numTelefone = numTelefone;
    }
    
    
    
    
}
