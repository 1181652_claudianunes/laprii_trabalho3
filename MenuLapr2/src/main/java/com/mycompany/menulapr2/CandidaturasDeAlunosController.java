/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.menulapr2;

import de.jensd.fx.glyphs.materialdesignicons.MaterialDesignIconView;
import java.io.IOException;
import static java.lang.Integer.parseInt;
import java.net.URL;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author ProxyOne
 */
public class CandidaturasDeAlunosController implements Initializable {

    @FXML
    private TextField txtNomeAluno;
    @FXML
    private TextField txtNIC;
    @FXML
    private TextField txtNIF;
    @FXML
    private TextField txtRua;
    @FXML
    private TextField txtCodigoPostal;
    @FXML
    private TextField txtContatoTelefonico;
    @FXML
    private TextField txtEmail;
    @FXML
    private ComboBox<String> comboBoxCurso;
    @FXML
    private Button buttonVoltar;
    @FXML
    private Button buttonConfirmar;
    @FXML
    private Button buttonApagar;
    @FXML
    private ComboBox<String> comboBoxCursoRegime;
    @FXML
    private Label lblNVagas;
    private Label lblNome;
    @FXML
    private MaterialDesignIconView iconConfirmar;
    @FXML
    private MaterialDesignIconView iconReset;
    @FXML
    private MaterialDesignIconView iconVoltar;

    
    private int numVagasLivres;
    
    private Utilizador utilizadorAtual;
    


    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        fillComboCurso();
        verifIntegridade();

        lblNVagas.setText("");
    }
    
    public void dadosInicio(Utilizador utilizador) {
        utilizadorAtual = utilizador;
    }

    @FXML
    public void goToAdministracao(ActionEvent e) throws IOException {
        if (Alertas.alertaConfirmacao("Confirmação", "Deseja voltar para a zona de Administração? Se escolher OK, todos os dados que não foram gravados vão ser apagados").getResult() == ButtonType.OK) {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("/fxml/Administracao.fxml"));
            Parent root = loader.load();
            
            AdministracaoController controller = loader.getController();
            controller.initData(utilizadorAtual); //passar o objeto Utilizador para a próxima Scene;

            Scene scene = new Scene(root);
            scene.getStylesheets().add("/styles/Styles.css");

            Stage stage = (Stage) ((Node) e.getSource()).getScene().getWindow();
            stage.setTitle("Escola de formação Evaristo");
            stage.setScene(scene);
            stage.show();

        }
    }

    public void verifIntegridade() {

        String regex = "^[\\p{L}a-zA-Z ]*$";
        String regex2 = "^[0-9]{1,8}$";
        String regex3 = "^[0-9]{1,9}$";
        String regex4 = "^[0-9]{4}-[0-9]{3}$";
        String regex5 = ".+\\@.+\\..+";

        txtNomeAluno.textProperty().addListener((observable, oldValue, newValue) -> {
            if (!txtNomeAluno.getText().matches(regex)) {
                txtNomeAluno.deleteNextChar();
                Alertas.alertaErro("Erro - Caracter numérico", "No campo nome apenas são permitidos carateres alfanuméricos");
            } else {
                if (txtNomeAluno.getText().isEmpty() == false && txtNomeAluno.getText().length() > 51) {
                    txtNomeAluno.deleteNextChar();
                    Alertas.alertaErro("Erro - Excedeu o número de caracteres", "No campo nome é permitido até 50 caracteres");
                }
            }
        });

        txtNIC.textProperty().addListener((observable, oldValue, newValue) -> {
            if (txtNIC.getText().isEmpty() == false && !txtNIC.getText().matches(regex2)) {
                txtNIC.deleteNextChar();
                Alertas.alertaErro("Erro - Formato do NIC", "NIC deve ter 8 digitos");
            }
        });
        txtNIC.focusedProperty().addListener((observable, oldValue, newValue) -> {
            if (txtNIC.getText().isEmpty() == false && txtNIC.getText().length() != 8) {
                txtNIC.setText("");
                Alertas.alertaErro("Erro - Formato do NIC", "NIC deve ter 8 digitos");
            }
        });
        txtNIF.textProperty().addListener((observable, oldValue, newValue) -> {
            if (txtNIF.getText().isEmpty() == false && !txtNIF.getText().matches(regex3)) {
                txtNIF.deleteNextChar();
                Alertas.alertaErro("Erro - Formato do NIF", "NIF deve ter 9 digitos");
            }
        });
        txtNIF.focusedProperty().addListener((observable, oldValue, newValue) -> {
            if (txtNIF.getText().isEmpty() == false && txtNIF.getText().length() != 9) {
                txtNIF.setText("");
                Alertas.alertaErro("Erro - Formato do NIF", "NIF deve ter 9 digitos");
            }
        });
        txtRua.textProperty().addListener((observable, oldValue, newValue) -> {
            if (txtRua.getText().isEmpty() == false && txtRua.getText().length() > 51) {
                txtRua.deleteNextChar();
                Alertas.alertaErro("Erro - Excedeu o número de caracteres", "No campo Rua é permitido até 50 caracteres");
            }

        });
        txtCodigoPostal.focusedProperty().addListener((observable, oldValue, newValue) -> {
            if (txtCodigoPostal.getText().isEmpty() == false && !txtCodigoPostal.getText().matches(regex4)) {
                txtCodigoPostal.setText("");
                Alertas.alertaErro("Erro - Formato do código postal", "Formato do código postal deve ser 9999-999");
            }
        });

        txtContatoTelefonico.textProperty().addListener((observable, oldValue, newValue) -> {
            if (txtContatoTelefonico.getText().isEmpty() == false && !txtContatoTelefonico.getText().matches(regex3)) {
                txtContatoTelefonico.deleteNextChar();
                Alertas.alertaErro("Erro - Formato do contacto telefónico", "Contacto telefónico deve ter 9 digitos");
            }
        });
        txtContatoTelefonico.focusedProperty().addListener((observable, oldValue, newValue) -> {
            if (txtContatoTelefonico.getText().isEmpty() == false && txtContatoTelefonico.getText().length() != 9) {
                txtContatoTelefonico.setText("");
                Alertas.alertaErro("Erro - Formato do contacto telefónico", "Contacto telefónico deve ter 9 digitos");
            }
        });
        txtEmail.focusedProperty().addListener((observable, oldValue, newValue) -> {
            if (txtEmail.getText().isEmpty() == false && !txtEmail.getText().matches(regex5)) {
                txtEmail.setText("");
                Alertas.alertaErro("Erro - Formato do Email não é válido", "Por favor insira um Email válido");
            }
        });

    }

    @FXML
    public void registarCandidatura() {

        System.out.println(numVagasLivres);
        if (numVagasLivres < 1) {
            System.out.println("erro nao tem vagas");
            Alertas.alertaConfirmacao("Não existe vagas", "Este curso não tem vagas livres");

        } else {

//        //regista na tabela aluno
            try {
                Statement query = Conexao.connectarBD().createStatement();
                String nomeUpdateQuery1 = "INSERT INTO Aluno (nome_completo, NIC, NIF, rua, codigo_postal, contacto_telefonico, email)";
                String nomeUpdateQuery2 = " VALUES ('" + txtNomeAluno.getText() + "'," + txtNIC.getText() + "," + txtNIF.getText() + ",'" + txtRua.getText() + "','" + txtCodigoPostal.getText() + "'," + txtContatoTelefonico.getText() + ",'" + txtEmail.getText() + "')";
                query.executeUpdate(nomeUpdateQuery1 + nomeUpdateQuery2);

            } catch (SQLException e) {
                e.getMessage();
            }

            //regista na tabela candidatura
            try {
                Statement query = Conexao.connectarBD().createStatement();
                ResultSet rs = query.executeQuery("SELECT cod_curso FROM Curso WHERE nome like '" + comboBoxCurso.valueProperty().get() + "' and regime like '" + comboBoxCursoRegime.valueProperty().get() + "'");
                String codCurso = null;

                while (rs.next()) {
                    codCurso = rs.getString("cod_curso");
                }

                String nomeUpdateQuery1 = "INSERT INTO Candidatura (cod_curso, nic, dataCandidatura) ";
                String nomeUpdateQuery2 = "VALUES (" + codCurso + ",'" + txtNIC.getText() + "', current_timestamp)";
                query.executeUpdate(nomeUpdateQuery1 + nomeUpdateQuery2);
                Alertas.alertaConfirmacao("Dados registados", "Os dados foram registados com sucesso");
                apagarDados();

            } catch (SQLException e) {
                e.getMessage();
            }
        }
    }

//    tem que ser alterado para valores da base de dados
    public void fillComboCurso() {
        ObservableList<String> listaCursos = FXCollections.observableArrayList();
        try {
            Statement query = Conexao.connectarBD().createStatement();
            ResultSet rs = query.executeQuery("select distinct nome from Curso order by nome asc");
            while (rs.next()) {
                String cursoTemp = rs.getString("nome");
                listaCursos.add(cursoTemp);
            }
            comboBoxCurso.setItems(listaCursos);
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    @FXML
    public void comboCurso() {
        String strTemp = "'" + comboBoxCurso.getValue() + "'";
        ObservableList<String> listaRegimesCurso = FXCollections.observableArrayList();
        try {
            Statement query = Conexao.connectarBD().createStatement();
            ResultSet rs = query.executeQuery("select regime from Curso where nome like " + strTemp + "order by regime asc");
            while (rs.next()) {
                String cursoTemp = rs.getString("regime");
                listaRegimesCurso.add(cursoTemp);
            }
            comboBoxCursoRegime.setItems(listaRegimesCurso);
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    @FXML
    public void apagarDados() {
        if (Alertas.alertaConfirmacao("Confirmação - Apagar Dados", "Clique em OK para apagar todos os dados").getResult() == ButtonType.OK) {
            txtNomeAluno.setText("");
            txtNIC.setText("");
            txtNIF.setText("");
            txtRua.setText("");
            txtCodigoPostal.setText("");
            txtContatoTelefonico.setText("");
            txtEmail.setText("");
            lblNVagas.setText("");
            comboBoxCurso.setPromptText("Cursos em vigor");
            comboBoxCursoRegime.getItems().clear();
            lblNome.setText("");
            fillComboCurso();
        }
    }

    @FXML
    public void nvagas() {
        int tVagas = 0;
        int nInscr = 0;

        try {
            Statement query = Conexao.connectarBD().createStatement();
            ResultSet rs = query.executeQuery("select count(cod_curso) as numero_inscr from Candidatura where cod_curso = (select cod_curso from Curso where nome like '" + comboBoxCurso.getValue() + "' and regime like '" + comboBoxCursoRegime.getValue() + "')");
            while (rs.next()) {
                nInscr = parseInt(rs.getString("numero_inscr"));
            }

            ResultSet rs2 = query.executeQuery("select numero_vagas from Curso where nome like '" + comboBoxCurso.getValue() + "' and regime like '" + comboBoxCursoRegime.getValue() + "'");
            while (rs2.next()) {
                tVagas = parseInt(rs2.getString("numero_vagas"));
            }

            lblNVagas.setText((tVagas - nInscr) + " Vagas de " + tVagas);
            numVagasLivres = (tVagas - nInscr);
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }
}
