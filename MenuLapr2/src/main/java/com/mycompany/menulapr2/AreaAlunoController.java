package com.mycompany.menulapr2;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;

/**
 * FXML Controller class
 *
 * @author Cláudia Nunes
 */
public class AreaAlunoController implements Initializable {

    @FXML
    private Label lblEscolaFormacao;
    @FXML
    private Label lblEvaristo;
    @FXML
    private Label lblBemVindo;
    @FXML
    private Button btnCandidaturaCursos;
    @FXML
    private Label lblAreaCursos;
    @FXML
    private Button btnVisualizarHorarios;
    @FXML
    private Label lblAreaHorarios;
    @FXML
    private Button btnConsultarNotas;
    @FXML
    private Label lblAreaNotas;
    @FXML
    private Button btnVisualizarSumarios;
    @FXML
    private Label lblAreaSumarios;
    @FXML
    private Button btnVisualizarDataExames;
    @FXML
    private Label lblAreaExames;
    @FXML
    private Label lblNomeUtilizador;
    @FXML
    private Button btnTerminarSessao;

    private Utilizador loggedUtilizador = null;
   

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }

    public void initData(Utilizador utilizador) {
        this.loggedUtilizador = utilizador;

        if (this.loggedUtilizador == null) {
            return;
        }
    }
    
    @FXML
    private void terminarSessao(ActionEvent event) {
        String resultado = Alertas.alertaConfirmacao("Confirmação - Terminar Sessão", "Tem a certeza que pretende terminar sessão?").getResult().getText();

        if (resultado.equalsIgnoreCase("OK")) {
            System.exit(0);
        }
    }

}
