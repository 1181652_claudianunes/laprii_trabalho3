package com.mycompany.menulapr2;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;

/**
 * FXML Controller class
 *
 * @author Cláudia Nunes
 */
public class AreaFormadorController implements Initializable {

    @FXML
    private Label lblEscolaFormacao;
    @FXML
    private Label lblEvaristo;
    @FXML
    private Label lblBemVindo;
    @FXML
    private Button btnVisualizarHorarios;
    @FXML
    private Button btnInserirFaltas;
    @FXML
    private Button btnInserirSumarios;
    @FXML
    private Label lblAreaHorarios;
    @FXML
    private Label lblAreaSumarios;
    @FXML
    private Label lblAreaFaltas;
    @FXML
    private Button btnInserirNotas;
    @FXML
    private Button btnInserirNotasExames;
    @FXML
    private Label lblAreaNotas;
    @FXML
    private Label lblNomeUtilizador;
    @FXML
    private Button btnTerminarSessao;

    private Utilizador user;
    

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }

    public void initData(Utilizador utilizador) {
        user = utilizador;

        if (user == null) {
            return;
        }

        lblNomeUtilizador.setText(user.getNomeUtilizador());
    }
    
    @FXML
    private void terminarSessao(ActionEvent event) {
        String resultado = Alertas.alertaConfirmacao("Confirmação - Terminar Sessão", "Tem a certeza que pretende terminar sessão?").getResult().getText();

        if (resultado.equalsIgnoreCase("OK")) {
            System.exit(0);
        }
    }

}
