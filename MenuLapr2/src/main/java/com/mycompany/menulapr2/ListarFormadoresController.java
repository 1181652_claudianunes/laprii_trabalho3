package com.mycompany.menulapr2;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import de.jensd.fx.glyphs.materialdesignicons.MaterialDesignIconView;
import java.io.IOException;
import java.net.URL;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author Cláudia Nunes
 */
public class ListarFormadoresController implements Initializable {

    @FXML
    private TableView<Formador> tableViewFormador;
    @FXML
    private TableColumn<Formador, Integer> colunaNifFormador;
    @FXML
    private TableColumn<Formador, String> colunaNomeCompleto;
    @FXML
    private TableColumn<Formador, String> colunaRua;
    @FXML
    private TableColumn<Formador, String> colunaCodPostal;
    @FXML
    private TableColumn<Formador, Integer> colunaContactoTelefonico;
    @FXML
    private Button buttonVoltarAtras1;
    @FXML
    private MaterialDesignIconView iconVoltarAtras1;
    
    private Utilizador utilizadorAtual;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        mostrarDados();
        preencherView();
    }
    
    public void dadosInicio(Utilizador utilizador) {
        utilizadorAtual = utilizador;
    }


    public void mostrarDados() {
        colunaNifFormador.setCellValueFactory(new PropertyValueFactory<>("nif"));
        colunaNomeCompleto.setCellValueFactory(new PropertyValueFactory<>("nomeCompleto"));
        colunaRua.setCellValueFactory(new PropertyValueFactory<>("rua"));
        colunaCodPostal.setCellValueFactory(new PropertyValueFactory<>("codPostal"));
        colunaContactoTelefonico.setCellValueFactory(new PropertyValueFactory<>("numTelefone"));
    }

    public void preencherView() {

        ObservableList<Formador> listaFormadores = FXCollections.observableArrayList();
        try {
            Statement query = Conexao.connectarBD().createStatement();
            ResultSet rs = query.executeQuery("SELECT * FROM Formador");

            while (rs.next()) {

                Formador formador = new Formador(rs.getInt("NIF"), rs.getString("nome_completo"), rs.getString("rua"), rs.getString("cod_postal"), rs.getInt("numero_telefone"));
                listaFormadores.add(formador);
            }

        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        tableViewFormador.setItems(listaFormadores);
        tableViewFormador.setEditable(true);
    }

    @FXML
    public void voltarAtras(ActionEvent e) throws IOException {
        if (Alertas.alertaConfirmacao("Voltar para página anterior", "Se clica em OK irá voltar para a página anterior").getResult() == ButtonType.OK) {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("/fxml/Administracao.fxml"));
            Parent root = loader.load();
            
            AdministracaoController controller = loader.getController();
            controller.initData(utilizadorAtual); //passar o objeto Utilizador para a próxima Scene;

            Scene scene = new Scene(root);
            scene.getStylesheets().add("/styles/Styles.css");

            Stage stage = (Stage) ((Node) e.getSource()).getScene().getWindow();
            stage.setTitle("Gestão de Escola de Formação");
            stage.setScene(scene);
            stage.show();
        }
    }
}
