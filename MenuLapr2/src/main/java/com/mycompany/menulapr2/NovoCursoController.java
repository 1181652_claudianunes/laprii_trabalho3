package com.mycompany.menulapr2;

/*
  * To change this license header, choose License Headers in Project Properties.
  * To change this template file, choose Tools | Templates
  * and open the template in the editor.
 */
import de.jensd.fx.glyphs.materialdesignicons.MaterialDesignIconView;
import java.io.IOException;
import static java.lang.Double.parseDouble;
import static java.lang.Integer.parseInt;
import java.net.URL;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author Goldb
 */
public class NovoCursoController implements Initializable {

    @FXML
    private Label lblNomeCurso;
    @FXML
    private Label lblNumVagas;
    @FXML
    private Label lblPreco;
    @FXML
    private Label lblAreaEnsino;
    @FXML
    private Label lblDuracaoCurso;
    @FXML
    private TextField txtNomeCurso;
    @FXML
    private TextField txtNumVagas;
    @FXML
    private TextField txtPrecoCurso;
    @FXML
    private TextField txtAreaEnsino;
    @FXML
    private TextField txtDuracaoCurso;
    @FXML
    private Button buttonConfirmar;
    @FXML
    private MaterialDesignIconView iconConfirmar;
    @FXML
    private Button buttonReset;
    @FXML
    private MaterialDesignIconView iconReset;
    @FXML
    private MaterialDesignIconView iconVoltar;
    @FXML
    private Button buttonVoltarAtras;
    @FXML
    private ComboBox comboBoxRegime;
    @FXML
    private TextField txtVagasMinimas;
    @FXML
    private Label lblRegime;
    @FXML
    private Label lblMeses;
    @FXML
    private Label lblVagasMinimas;
    
    private Utilizador utilizadorAtual;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        propriedadesIniciais();

        txtDuracaoCurso.textProperty().addListener((valorDuracao, valorAntigo, novoValor) -> {

            if (txtDuracaoCurso.getText().isEmpty() == false && parseInt(txtDuracaoCurso.getText()) > 60) {
                txtDuracaoCurso.deleteNextChar();
                Alertas.alertaErro("Erro - Duração de Curso", "A duração máxima de um curso é de 60 meses");
            }
        });

        txtNumVagas.textProperty().addListener((valorNumVagas, valorAntigo, novoValor) -> {

            if (txtNumVagas.getText().isEmpty() == false && parseInt(txtNumVagas.getText()) > 40) {
                txtNumVagas.deleteNextChar();
                Alertas.alertaErro("Erro - Número de Vagas", "O número máximo de vagas por curso é 40");
            }
        });
    }
    
    public void dadosInicio(Utilizador utilizador) {
        utilizadorAtual = utilizador;
    }

    public void propriedadesIniciais() {

        ObservableList<String> itemsComboBox = FXCollections.observableArrayList();
        itemsComboBox.add("Pós-Laboral");
        itemsComboBox.add("Laboral");
        comboBoxRegime.setItems(itemsComboBox);
    }

    @FXML
    public void goToAdministracao(ActionEvent e) throws IOException {

        if (Alertas.alertaConfirmacao("Confirmação", "Deseja voltar para a zona de Administração? Se escolher OK, todos os dados que não foram gravados vão ser apagados").getResult() == ButtonType.OK) {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("/fxml/Administracao.fxml"));
            Parent root = loader.load();
            
            AdministracaoController controller = loader.getController();
            controller.initData(utilizadorAtual); //passar o objeto utilizador para a próxima Scene;

            Scene scene = new Scene(root);
            scene.getStylesheets().add("/styles/Styles.css");

            Stage stage = (Stage) ((Node) e.getSource()).getScene().getWindow();
            stage.setTitle("Sistema de Gestão de Hoteis");
            stage.setScene(scene);
            stage.show();
        }
    }

    @FXML
    public void apagarDados(ActionEvent e) {

        if (Alertas.alertaConfirmacao("Confirmação - Apagar Dados", "Clique em OK para apagar todos os dados").getResult() == ButtonType.OK) {

            txtNomeCurso.setText("");
            txtNumVagas.setText("");
            txtVagasMinimas.setText("");
            txtPrecoCurso.setText("");
            txtAreaEnsino.setText("");
            txtDuracaoCurso.setText("");

            comboBoxRegime.getItems().clear();
            ObservableList<String> itemsComboBox = FXCollections.observableArrayList();
            itemsComboBox.add("Pós-Laboral");
            itemsComboBox.add("Laboral");
            comboBoxRegime.setItems(itemsComboBox);
        }
    }

    /**
     * Método para o campo [NOME] e [ÁREA DE ENSINO] Evento do tipo KeyEvent
     * para garantir que no campo Nome, só são introduzidos as letras
     * convencionais para nomes
     */
    @FXML
    public void integridadeCampoLirico1(javafx.scene.input.KeyEvent event) {
        String caracterEscrito = event.getCharacter();

        for (int i = 0; i < caracterEscrito.length(); i++) {
            Character c = caracterEscrito.charAt(i);

            if (Character.isDigit(c) || !Character.isLetter(c)) { //se o caracter introduzido for um número ou não for uma letra, este será "consumido"
                if (Character.isSpaceChar(c)) {
                    /*
                     o primeiro ciclo testa se o caracter introduzido é um digito e se não é uma letra, ou seja, testa
                     se é um cáracter tipo: @ - , . / ,etc. e também testa se é um espaço em branco, no entanto, se for
                     um espaço branco, têm que se abrir uma exceção pois o campo Nome , recebe o nome completo do cliente,
                     o que indica que vai ser preciso espaços. Para isso, criamos um segundo ciclo if, para estar se é um
                     espaço, se for um espaço, abre uma exceção e deixa o funcionário introduzir, senão, passa para o ciclo
                     else em baixo e "consome" todos as "não-letras"
                     */
                } else {
                    event.consume();
                }
            }
        }
    }

    /**
     * Método para o campo [PREÇO DO CURSO] Evento do tipo KeyEvent para
     * garantir que no campo Nome, só são introduzidos as letras convencionais
     * para nomes
     */
    @FXML
    public void integridadeCampoLirico2(javafx.scene.input.KeyEvent event) {

        String caracterEscrito = event.getCharacter();

        if (txtPrecoCurso.getText().isEmpty() == false) {
            try {

                Double.parseDouble(txtPrecoCurso.getText());

                for (int i = 0; i < caracterEscrito.length(); i++) {

                    Character c = caracterEscrito.charAt(i);

                    if (!Character.isDigit(c)) { //se o caracter introduzido for um número ou não for uma letra, este será "consumido"

                        if (c == '.') {
                            /*
                             o primeiro ciclo testa se o caracter introduzido é um digito e se não é uma letra, ou seja, testa
                             se é um cáracter tipo: @ - , . / ,etc. e também testa se é um ".", no entanto, se for
                             um ".", têm que se abrir uma exceção pois o campo Preço , recebe números reais,
                             o que indica que vai ser preciso ".". Para isso, criamos um segundo ciclo if, para estar se é um
                             ".", se for um ".", abre uma exceção e deixa o Administrador introduzir, senão, passa para o ciclo
                             else em baixo e "consome" todas as letras
                             */

                        } else {

                            event.consume();
                        }
                    }
                }
            } catch (NumberFormatException e) {
                Alertas.alertaErro("Erro - Preço do Curso inválido", "O valor do Preço do Curso só pode ter um caracter de separação. Introduza um valor válido");
                txtPrecoCurso.setText("");
            }
        }
    }

    /**
     * Evento do tipo KeyEvent para garantir que nos campos numéricos, só são
     * introduzidos números e que o número 0 não é introduzido. Todos as letras
     * que o utilizador tente escrever são "consumidas", ou seja, não aparecem
     * no ecrã e são descartadas
     */
    public void integridadeCampoNumerico1(javafx.scene.input.KeyEvent event) {

        String caracterEscrito = event.getCharacter();

        for (int i = 0; i < caracterEscrito.length(); i++) {

            Character c = caracterEscrito.charAt(i);

            if (txtDuracaoCurso.textProperty().isEmpty().getValue() == false) { // se a textField não estiver vazia, é permitido introduzir o valor 0, para números como: 10,20,etc.

                if (!Character.isDigit(c)) {
                    event.consume();
                }

            } else { //se a textField estiver vazia, não é permitido introduzir o valor 0, pois o minimo número de pessoas é 1

                if (!Character.isDigit(c) || c == '0') {
                    event.consume();
                }
            }
        }
    }

    /**
     * Evento do tipo KeyEvent para garantir que nos campos numéricos, só são
     * introduzidos números e que o número 0 não é introduzido. Todos as letras
     * que o utilizador tente escrever são "consumidas", ou seja, não aparecem
     * no ecrã e são descartadas
     */
    @FXML
    public void integridadeCampoNumerico2(javafx.scene.input.KeyEvent event) {

        String caracterEscrito = event.getCharacter();

        for (int i = 0; i < caracterEscrito.length(); i++) {

            Character c = caracterEscrito.charAt(i);

            if (txtNumVagas.textProperty().isEmpty().getValue() == false) { // se a textField não estiver vazia, é permitido introduzir o valor 0, para números como: 10,20,etc.

                if (!Character.isDigit(c)) {
                    event.consume();
                }
            } else { //se a textField estiver vazia, não é permitido introduzir o valor 0, pois o minimo número de pessoas é 1

                if (!Character.isDigit(c) || c == '0') {
                    event.consume();
                }
            }
        }
    }

    @FXML
    public void registarCurso(ActionEvent e) {

        try {
            if (txtPrecoCurso.getText().isEmpty() == false) {
                System.out.println(txtPrecoCurso.getText());
                Double.parseDouble(txtPrecoCurso.getText());
                //break updateBaseDeDados;

            }
        } catch (NumberFormatException ex) {
            Alertas.alertaErro("Erro - Preço do Curso inválido", "O valor do Preço do Curso só pode ter um caracter de separação. Introduza um valor válido");
        }

        if (txtVagasMinimas.getText().isEmpty() == false && parseInt(txtVagasMinimas.getText()) < 10) {
            System.out.println("TESTE VAGAS");
            Alertas.alertaErro("Erro - Número Minimo de Vagas", "O Curso têm que ter no minimo 10 alunos para pode ser lecionado");
            //break updateBaseDeDados;        
        }

        if (txtVagasMinimas.getText().isEmpty() == true || txtPrecoCurso.getText().isEmpty() == true || txtNomeCurso.getText().isEmpty() == true || txtNumVagas.getText().isEmpty() == true || txtPrecoCurso.getText().isEmpty() == true || txtAreaEnsino.getText().isEmpty() == true || txtDuracaoCurso.getText().isEmpty() == true || comboBoxRegime.valueProperty().isNull().getValue() == true) {
            System.out.println("TESTE VAGAS2");
            Alertas.alertaErro("Erro - Campos vazios", "Todos os campos têm que ser preenchidos para fazer um registo");
            //break updateBaseDeDados;
        } else {

            try {

                String primeiraParteQuery = "INSERT INTO Curso(area_ensino,duracao_curso,numero_vagas,preco,regime,vagasMinimas,nome)";
                String segundaParteQuery = " VALUES('" + txtAreaEnsino.getText() + "'," + parseInt(txtDuracaoCurso.getText()) + "," + parseInt(txtNumVagas.getText()) + "," + parseDouble(txtPrecoCurso.getText()) + ",'" + comboBoxRegime.valueProperty().get() + "'," + parseInt(txtVagasMinimas.getText()) + ",'" + txtNomeCurso.getText() + "')";
                Statement query = Conexao.connectarBD().createStatement();
                query.executeUpdate(primeiraParteQuery + segundaParteQuery);
                System.out.println(primeiraParteQuery + segundaParteQuery);

            } catch (SQLException exception) {
                System.out.println(exception.getMessage());
            }
        }
    }
}
