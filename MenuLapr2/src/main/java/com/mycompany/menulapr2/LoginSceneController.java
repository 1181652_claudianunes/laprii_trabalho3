package com.mycompany.menulapr2;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyEvent;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author Cláudia Nunes
 */
public class LoginSceneController implements Initializable {

    @FXML
    private TextField txtUserName;
    @FXML
    private TextField txtPassword;
    @FXML
    private Button btnLogin;
    @FXML
    private Label lblEscolaFormacao;
    @FXML
    private Label lblEvaristo;
    @FXML
    private Label lblUserName;
    @FXML
    private Label lblPassword;
    @FXML
    private ComboBox<String> comboBoxTipoUtilizador;
    @FXML
    private Label lblTipoUtilizador;
    
    
    private Connection con = null;
    


    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        con = Conexao.connectarBD();
        propriedadesIniciais();
    }

    @FXML
    private void handleLogin(ActionEvent event) {
        login();
    }

    private void handleOnKeyPressed(KeyEvent event) {
        if (event.getCode().toString().equals("ENTER")) {
            login();
        }
    }

    private void login() {
        String userName = txtUserName.getText();
        if (userName == null || userName.isEmpty()) {
            Alertas.alertaInformacao("Informação", "Username é de preenchimento obrigatório.");
            return;
        }

        String password = txtPassword.getText();
        if (password == null || password.isEmpty()) {
            Alertas.alertaInformacao("Informação", "Password é de preenchimento obrigatório.");
            return;
        }

        if (comboBoxTipoUtilizador.valueProperty().isNull().getValue() == true) {
            Alertas.alertaInformacao("Informação", "Tipo de Utilizador é de preenchimento obrigatório.");
            return;
        }

        String query = "SELECT * FROM Utilizador WHERE username = ? AND password = ? AND tipoUtilizador = ?";

        try {
            PreparedStatement preparedStatement = null;
            ResultSet resultSet = null;

            preparedStatement = con.prepareStatement(query);
            preparedStatement.setString(1, userName);
            preparedStatement.setString(2, password);
            preparedStatement.setString(3, String.valueOf(comboBoxTipoUtilizador.getValue()));

            resultSet = preparedStatement.executeQuery();
            if (!resultSet.next()) {
                Alertas.alertaErro("Erro", "Username/Password/TipoUtilizador errados!");
                return;
            } else {
                Utilizador utilizador = new Utilizador(resultSet.getInt("idUtilizador"),resultSet.getString("nomeUtilizador"), resultSet.getString("userName"));
                if (comboBoxTipoUtilizador.getValue() == "Administrador") {
                    abrirPainelAdministracao(utilizador);
                } else if (comboBoxTipoUtilizador.getValue() == "Formador") {
                    abrirPainelFormador(utilizador);
                } else {
                    if (comboBoxTipoUtilizador.getValue() == "Aluno") {
                        abrirPainelAluno(utilizador);
                    }
                }

            }
        } catch (Exception e) {
            Logger.getLogger(LoginSceneController.class.getName()).log(Level.SEVERE, null, e);
        }

    }

    public void propriedadesIniciais() {
        ObservableList<String> itemsComboBox = FXCollections.observableArrayList();
        itemsComboBox.add("Administrador");
        itemsComboBox.add("Formador");
        itemsComboBox.add("Aluno");
        comboBoxTipoUtilizador.setItems(itemsComboBox);
    }

    private void abrirPainelAdministracao(Utilizador user) throws IOException {
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/fxml/Administracao.fxml"));
        Parent root = loader.load();

        Scene scene = new Scene(root);
        scene.getStylesheets().add("/styles/Styles.css");

        AdministracaoController controller = loader.getController();
        controller.initData(user);
        
        Stage stage = (Stage) btnLogin.getScene().getWindow();
        stage.setTitle("Área de Administrador - Escola de Formação e-Varisto");
        stage.setScene(scene);
        stage.show();
    }

    private void abrirPainelFormador(Utilizador user) throws IOException {
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/fxml/AreaFormador.fxml"));
        Parent root = loader.load();

        AreaFormadorController controller = loader.getController();
        controller.initData(user);
        Scene scene = new Scene(root);
        scene.getStylesheets().add("/styles/Styles.css");

        Stage stage = (Stage) btnLogin.getScene().getWindow();
        stage.setTitle("Área de Formador - Escola de Formação e-Varisto");
        stage.setScene(scene);
        stage.show();
    }

    private void abrirPainelAluno(Utilizador user) throws IOException {
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/fxml/AreaAluno.fxml"));
        Parent root = loader.load();

        AreaAlunoController controller = loader.getController();
        controller.initData(user);
        Scene scene = new Scene(root);
        scene.getStylesheets().add("/styles/Styles.css");

        Stage stage = (Stage) btnLogin.getScene().getWindow();
        stage.setTitle("Área de Aluno - Escola de Formação e-Varisto");
        stage.setScene(scene);
        stage.show();
    }

    
}
