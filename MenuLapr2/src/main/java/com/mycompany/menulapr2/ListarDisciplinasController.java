/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.menulapr2;

import de.jensd.fx.glyphs.materialdesignicons.MaterialDesignIconView;
import java.io.IOException;
import java.net.URL;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author Rafael Granja
 */
public class ListarDisciplinasController implements Initializable {

    @FXML
    private TableView<Disciplinas> tblViewDisciplina;
    @FXML
    private TableColumn<Disciplinas, String> colunaNomeDisciplina;
    @FXML
    private TableColumn<Disciplinas, String> colunaDescricao;
    @FXML
    private Button buttonVoltarAtras;
    @FXML
    private MaterialDesignIconView iconVoltarAtras;
    
    private Utilizador utilizadorAtual;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        mostrarResultados();
        verDados();
    }
    
    public void dadosInicio(Utilizador utilizador) {
        utilizadorAtual = utilizador;
    }

    public void mostrarResultados() {
        colunaNomeDisciplina.setCellValueFactory(new PropertyValueFactory<>("nomeDisciplina"));
        colunaDescricao.setCellValueFactory(new PropertyValueFactory<>("descricao"));
    }

    public void verDados() {

        ObservableList<Disciplinas> listaDisciplinas = FXCollections.observableArrayList();
        try {
            Statement query = Conexao.connectarBD().createStatement();
            ResultSet result = query.executeQuery("SELECT nome, descricao FROM Disciplina");
            
            while (result.next()) {
                Disciplinas disciplinas = new Disciplinas(result.getString("nome"), result.getString("descricao"));
                listaDisciplinas.add(disciplinas);
            }

        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        tblViewDisciplina.setItems(listaDisciplinas);
        tblViewDisciplina.setEditable(true);
    }

    @FXML
    public void voltarAtras(ActionEvent e) throws IOException {
        if (Alertas.alertaConfirmacao("Voltar para página anterior", "Se clica em OK irá voltar para a página anterior").getResult() == ButtonType.OK) {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("/fxml/Administracao.fxml"));
            Parent root = loader.load();
            
            AdministracaoController controller = loader.getController();
            controller.initData(utilizadorAtual); //passar o objeto Utilizador para a próxima Scene;

            Scene scene = new Scene(root);
            scene.getStylesheets().add("/styles/Styles.css");

            Stage stage = (Stage) ((Node) e.getSource()).getScene().getWindow();
            stage.setTitle("Gestão de Escola de Formação");
            stage.setScene(scene);
            stage.show();
        }
    }

}
