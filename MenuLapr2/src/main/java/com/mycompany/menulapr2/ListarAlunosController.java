package com.mycompany.menulapr2;

import de.jensd.fx.glyphs.materialdesignicons.MaterialDesignIconView;
import java.io.IOException;
import java.net.URL;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author Cláudia Nunes
 */
public class ListarAlunosController implements Initializable {
    
    @FXML
    private TableView<Aluno> tableViewAluno;
    @FXML
    private TableColumn<Aluno, Integer> colunaNic;
    @FXML
    private TableColumn<Aluno, String> colunaNomeCompleto;
    @FXML
    private TableColumn<Aluno, Integer> colunaNif;
    @FXML
    private TableColumn<Aluno, String> colunaRua;
    @FXML
    private TableColumn<Aluno, String> colunaCodPostal;
    @FXML
    private TableColumn<Aluno, Integer> colunaContactoTelefonico;
    @FXML
    private TableColumn<Aluno, String> colunaEmail;
    @FXML
    private Button buttonVoltarAtras;
    @FXML
    private MaterialDesignIconView iconVoltarAtras;
    
    private Utilizador utilizadorAtual;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        mostrarDados();
        preencherView();
    }    
    
    public void dadosInicio(Utilizador utilizador) {
        utilizadorAtual = utilizador;
    }
    
    public void mostrarDados(){
        colunaNic.setCellValueFactory(new PropertyValueFactory<>("nic"));
        colunaNomeCompleto.setCellValueFactory(new PropertyValueFactory<>("nomeCompleto"));
        colunaNif.setCellValueFactory(new PropertyValueFactory<>("nif"));
        colunaRua.setCellValueFactory(new PropertyValueFactory<>("rua"));
        colunaCodPostal.setCellValueFactory(new PropertyValueFactory<>("codPostal"));
        colunaContactoTelefonico.setCellValueFactory(new PropertyValueFactory<>("contactoTelefonico"));
        colunaEmail.setCellValueFactory(new PropertyValueFactory<>("email"));
    }
    
    public void preencherView(){
        
        ObservableList<Aluno> listaAlunos = FXCollections.observableArrayList();
        try{
            Statement query = Conexao.connectarBD().createStatement();
            ResultSet rs = query.executeQuery("SELECT * FROM Aluno");
        
            while(rs.next()){
            
                Aluno aluno = new Aluno(rs.getInt("NIC"),rs.getString("nome_completo"),rs.getInt("NIF"),rs.getString("rua"),rs.getString("codigo_postal"),rs.getInt("contacto_telefonico"),rs.getString("email"));
                listaAlunos.add(aluno);
            }
            
        }catch(SQLException e){
            System.out.println(e.getMessage());
        }
        tableViewAluno.setItems(listaAlunos);
        tableViewAluno.setEditable(true);
    }
        
    @FXML
    public void voltarAtras(ActionEvent e) throws IOException{
        if(Alertas.alertaConfirmacao("Voltar para página anterior", "Se clica em OK irá voltar para a página anterior").getResult() == ButtonType.OK){
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("/fxml/Administracao.fxml"));
            Parent root = loader.load();
            
            AdministracaoController controller = loader.getController();
            controller.initData(utilizadorAtual); //passar o objeto Utilizador para a próxima Scene;

            Scene scene = new Scene(root);
            scene.getStylesheets().add("/styles/Styles.css");

            Stage stage = (Stage) ((Node) e.getSource()).getScene().getWindow();
            stage.setTitle("Gestão de Escola de Formação");
            stage.setScene(scene);
            stage.show();
        }
    }
}
