/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.menulapr2;

import com.mycompany.menulapr2.AdministracaoController;
import com.mycompany.menulapr2.Alertas;
import com.mycompany.menulapr2.Conexao;
import com.mycompany.menulapr2.Utilizador;
import de.jensd.fx.glyphs.materialdesignicons.MaterialDesignIconView;
import java.io.IOException;
import java.net.URL;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author Rafael Granja
 */
public class ListarSumariosController implements Initializable {

    @FXML
    private Button buttonVoltarAtras;
    @FXML
    private MaterialDesignIconView iconVoltarAtras;
    
    private Utilizador utilizadorAtual;
    @FXML
    private TableColumn<?, ?> colunaNomeDisciplina;
    @FXML
    private TableColumn<?, ?> colunaNif;
    @FXML
    private TableColumn<?, ?> colunaHoraInicio;
    @FXML
    private TableColumn<?, ?> colunaHoraFim;
    @FXML
    private TableColumn<?, ?> colunaDescricao;
    @FXML
    private TableView<?> tblViewSumarios;
    @FXML
    private TableColumn<?, ?> colunaDataAula;

    public void setUtilizadorAtual(Utilizador utilizadorAtual) {
        this.utilizadorAtual = utilizadorAtual;
    }

    public Utilizador getUtilizadorAtual() {
        return utilizadorAtual;
    }

    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        mostrarResultados();
        preencherView();
    }    

    
    public void dadosInicio(Utilizador utilizador) {
        utilizadorAtual = utilizador;
    }

    
    public void mostrarResultados(){
    
    colunaDescricao.setCellValueFactory(new PropertyValueFactory<>("descricao"));
    colunaNomeDisciplina.setCellValueFactory(new PropertyValueFactory<>("nomeDisciplina"));
    colunaHoraFim.setCellValueFactory(new PropertyValueFactory<>("horaFim"));
    colunaHoraInicio.setCellValueFactory(new PropertyValueFactory<>("horaInicio"));
    colunaNif.setCellValueFactory(new PropertyValueFactory<>("nif"));
    colunaDataAula.setCellValueFactory(new PropertyValueFactory<>("dataAula"));
    }
    
    public void preencherView() {
        ObservableList<Sumarios> listaSumarios = FXCollections.observableArrayList();
        try {

            Statement query = Conexao.connectarBD().createStatement();
            ResultSet resultados = query.executeQuery("SELECT Aula_Disciplina.NIF, Aula_Disciplina.dataAula, Aula_Disciplina.hora_inicio, Aula_Disciplina.hora_fim, Sumarios.descricao FROM Aula_Disciplina INNER JOIN Sumarios ON Aula_Disciplina.NIF=Sumarios.NIF");
            while (resultados.next()) {
                Turma sumarios = new Sumarios(resultados.getString("nomeDisciplina"), resultados.getString("dataAula"), resultados.getString("horaInicio"), resultados.getString("horaFim"), resultados.getString("descricao"), resultados.getString("nif"));
                listaSumarios.add(sumarios);
            }

        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }

        tblViewSumarios.setItems(listaSumarios);
        tblViewSumarios.setEditable(true);
    }
    
    
    @FXML
    public void voltarAtras(ActionEvent e) throws IOException {
        
    if (Alertas.alertaConfirmacao("Voltar para página anterior", "Se clica em OK irá voltar para a página anterior").getResult() == ButtonType.OK) {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("/fxml/Administracao.fxml"));
            Parent root = loader.load();
            
            AdministracaoController controller = loader.getController();
            controller.initData(utilizadorAtual); //passar o objeto Utilizador para a próxima Scene;

            Scene scene = new Scene(root);
            scene.getStylesheets().add("/styles/Styles.css");

            Stage stage = (Stage) ((Node) e.getSource()).getScene().getWindow();
            stage.setTitle("Gestão de Escola de Formação");
            stage.setScene(scene);
            stage.show();
        }
        
    }
    
}
