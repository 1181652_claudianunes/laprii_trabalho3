/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.menulapr2;

/**
 *
 * @author Rafael Granja
 */
public class Disciplinas {

    private int codDisciplina;
    private String nomeDisciplina;
    private String descricao;

    public Disciplinas(int codDisciplina, String nomeDisciplina, String descricao) {
        this.codDisciplina = codDisciplina;
        this.nomeDisciplina = nomeDisciplina;
        this.descricao = descricao;
    }
    
    public Disciplinas(String nomeDisciplina, String descricao) {
        this.nomeDisciplina = nomeDisciplina;
        this.descricao = descricao;
    }
    
    public Disciplinas(int codDisciplina, String nomeDisciplina) {
        this.codDisciplina = codDisciplina;
        this.nomeDisciplina = nomeDisciplina;
    }

    
    public int getCodDisciplina() {
        return codDisciplina;
    }

    public void setCodDisciplina(int codDisciplina) {
        this.codDisciplina = codDisciplina;
    }

    public String getNomeDisciplina() {
        return nomeDisciplina;
    }

    public void setNomeDisciplina(String nomeDisciplina) {
        this.nomeDisciplina = nomeDisciplina;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

}
