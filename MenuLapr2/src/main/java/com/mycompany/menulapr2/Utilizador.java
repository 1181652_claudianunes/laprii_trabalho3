/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.menulapr2;

/**
 *
 * @author Cláudia Nunes
 */
public class Utilizador {

    private int idUtilizador;
    private String nomeUtilizador;
    private String userName;
    private String password;
    private String tipoUtilizador;
    private boolean autenticado;

    public Utilizador() {
    }

    public Utilizador(int idUtilizador, String nomeUtilizador, String userName) {
        this.idUtilizador=idUtilizador;
        this.nomeUtilizador=nomeUtilizador;        
        this.userName = userName;
        }

    public int getIdUtilizador() {
        return idUtilizador;
    }

    public void setIdUtilizador(int idUtilizador) {
        this.idUtilizador = idUtilizador;
    }
    
    public String getNomeUtilizador() {
        return nomeUtilizador;
    }

    public void setNomeUtilizador(String nomeUtilizador) {
        this.nomeUtilizador = nomeUtilizador;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getTipoUtilizador() {
        return tipoUtilizador;
    }

    public void setTipoUtilizador(String tipoUtilizador) {
        this.tipoUtilizador = tipoUtilizador;
    }

    public boolean isAutenticado() {
        return autenticado;
    }

    public void setAutenticado(boolean autenticado) {
        this.autenticado = autenticado;
    }

    public boolean estaAutenticado(String userName) {
        return false;
    }

    public void autenticar(String userName, String password) {
    }
}
