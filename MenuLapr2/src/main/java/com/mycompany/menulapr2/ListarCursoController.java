package com.mycompany.menulapr2;

import de.jensd.fx.glyphs.materialdesignicons.MaterialDesignIconView;
import java.io.IOException;
import java.net.URL;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DecimalFormat;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;

public class ListarCursoController implements Initializable {

    @FXML
    private TableView<Curso> tableViewCurso;
    @FXML
    private TableColumn<Curso, Integer> colunaNumVagas;
    @FXML
    private TableColumn<Curso, Double> colunaPreco;
    @FXML
    private TableColumn<Curso, String> colunaRegime;
    @FXML
    private TableColumn<Curso, String> colunaAreaEnsino;
    @FXML
    private TableColumn<Curso, Integer> colunaDuracao;
    @FXML
    private TableColumn<Curso, String> colunaNomeCurso;
    @FXML
    private Button buttonVoltarAtras1;
    @FXML
    private MaterialDesignIconView iconVoltarAtras1;
    
    private Utilizador utilizadorAtual;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        propriedadesIniciais();
        setTableView();
    }
    
    public void dadosInicio(Utilizador utilizador) {
        utilizadorAtual = utilizador;
    }

    public void propriedadesIniciais() {
        colunaNumVagas.setCellValueFactory(new PropertyValueFactory<>("numeroVagas"));
        colunaPreco.setCellValueFactory(new PropertyValueFactory<>("preco"));
        colunaRegime.setCellValueFactory(new PropertyValueFactory<>("regime"));
        colunaAreaEnsino.setCellValueFactory(new PropertyValueFactory<>("areaEnsino"));
        colunaDuracao.setCellValueFactory(new PropertyValueFactory<>("duracaoCurso"));
        colunaNomeCurso.setCellValueFactory(new PropertyValueFactory<>("Nome"));
    }

    public void setTableView() {

        ObservableList<Curso> listaCursos = FXCollections.observableArrayList();
        try {
            Statement query = Conexao.connectarBD().createStatement();
            ResultSet rs = query.executeQuery("SELECT area_ensino,duracao_curso,nome,numero_vagas,round(preco,2) AS 'preco',regime FROM Curso");

            while (rs.next()) {
                DecimalFormat df = new DecimalFormat("#.##");
                String r = Double.toString(rs.getDouble("preco"));

                Curso curso = new Curso(rs.getString("area_ensino"), rs.getInt("duracao_curso"), rs.getString("nome"), rs.getInt("numero_vagas"), rs.getDouble("preco"), rs.getString("regime"));
                listaCursos.add(curso);
            }

        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        tableViewCurso.setItems(listaCursos);
        tableViewCurso.setEditable(true);
    }

    @FXML
    public void voltarAtras(ActionEvent e) throws IOException {
        if (Alertas.alertaConfirmacao("Voltar para página anterior", "Se clica em OK irá voltar para a página anterior").getResult() == ButtonType.OK) {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("/fxml/Administracao.fxml"));
            Parent root = loader.load();
            
            AdministracaoController controller = loader.getController();
            controller.initData(utilizadorAtual); //passar o objeto Utilizador para a próxima Scene;

            Scene scene = new Scene(root);
            scene.getStylesheets().add("/styles/Styles.css");

            Stage stage = (Stage) ((Node) e.getSource()).getScene().getWindow();
            stage.setTitle("Gestão de Escola de Formação");
            stage.setScene(scene);
            stage.show();
        }
    }

}
