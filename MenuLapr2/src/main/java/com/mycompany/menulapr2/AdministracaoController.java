package com.mycompany.menulapr2;

import java.awt.geom.Rectangle2D;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.stage.Screen;
import javafx.stage.Stage;

public class AdministracaoController implements Initializable {

    @FXML
    private Button buttonNovoCurso;
    @FXML
    private Button buttonListarCursos;
    @FXML
    private Button buttonNovaCandidatura;
    @FXML
    private Button buttonListarAlunos;
    @FXML
    private Button buttonNovaTurma;
    @FXML
    private Button buttonListarTurmas;
    @FXML
    private Button buttonNovaDisciplina;
    @FXML
    private Button buttonListarDisciplinas;
    @FXML
    private Button buttonNovoFormador;
    @FXML
    private Button buttonListarFormadores;
    @FXML
    private Button buttonNovoHorario;
    @FXML
    private Button buttonListarHorarios;
    @FXML
    private Label lblAreaCursos;
    @FXML
    private Label lblAreaAlunos;
    @FXML
    private Label lblAreaTurmas;
    @FXML
    private Label lblAreaDisciplinas;
    @FXML
    private Label lblAreaFormadores;
    @FXML
    private Label lblAreaHorarios;
    @FXML
    private Label lblEscolaFormacao;
    @FXML
    private Label lblEvaristo;
    @FXML
    private Label lblBemVindo;
    @FXML
    private Label lblNomeUtilizador;
    @FXML
    private Button btnTerminarSessao;
    @FXML
    private Label lblAreaSumarios;
    @FXML
    private Button buttonNovoSumario;
    @FXML
    private Button buttonListarSumários;

    private Utilizador user;
    
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }

    @FXML
    public void novoCurso(ActionEvent e) throws IOException {

        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/fxml/NovoCurso.fxml"));
        Parent root = loader.load();

        NovoCursoController controller = loader.getController();
        controller.dadosInicio(user);
        Scene scene = new Scene(root);
        scene.getStylesheets().add("/styles/Styles.css");

        Stage stage = (Stage) ((Node) e.getSource()).getScene().getWindow();
        stage.setTitle("Gestão de Escola de Formação");
        stage.setScene(scene);
        stage.show();

        javafx.geometry.Rectangle2D primScreenBounds = Screen.getPrimary().getVisualBounds();
        stage.setX((primScreenBounds.getWidth() - stage.getWidth()) / 2);
        stage.setY((primScreenBounds.getHeight() - stage.getHeight()) / 4);
    }

    @FXML
    public void listarCurso(ActionEvent e) throws IOException {
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/fxml/ListarCurso.fxml"));
        Parent root = loader.load();

        ListarCursoController controller = loader.getController();
        controller.dadosInicio(user);
        Scene scene = new Scene(root);
        scene.getStylesheets().add("/styles/Styles.css");

        Stage stage = (Stage) ((Node) e.getSource()).getScene().getWindow();
        stage.setTitle("Gestão de Escola de Formação");
        stage.setScene(scene);
        stage.show();

        javafx.geometry.Rectangle2D primScreenBounds = Screen.getPrimary().getVisualBounds();
        stage.setX((primScreenBounds.getWidth() - stage.getWidth()) / 2);
        stage.setY((primScreenBounds.getHeight() - stage.getHeight()) / 4);
    }

    @FXML
    public void novaCandidatura(ActionEvent e) throws IOException {

        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/fxml/CandidaturasDeAlunos.fxml"));
        Parent root = loader.load();

        CandidaturasDeAlunosController controller = loader.getController();
        controller.dadosInicio(user);
        Scene scene = new Scene(root);
        scene.getStylesheets().add("/styles/Styles.css");

        Stage stage = (Stage) ((Node) e.getSource()).getScene().getWindow();
        stage.setTitle("Gestão de Escola de Formação");
        stage.setScene(scene);
        stage.show();

        javafx.geometry.Rectangle2D primScreenBounds = Screen.getPrimary().getVisualBounds();
        stage.setX((primScreenBounds.getWidth() - stage.getWidth()) / 2);
        stage.setY((primScreenBounds.getHeight() - stage.getHeight()) / 4);
    }

    @FXML
    public void listarAlunos(ActionEvent e) throws IOException {
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/fxml/ListarAlunos.fxml"));
        Parent root = loader.load();

        ListarAlunosController controller = loader.getController();
        controller.dadosInicio(user);
        Scene scene = new Scene(root);
        scene.getStylesheets().add("/styles/Styles.css");

        Stage stage = (Stage) ((Node) e.getSource()).getScene().getWindow();
        stage.setTitle("Gestão de Escola de Formação");
        stage.setScene(scene);
        stage.show();

        javafx.geometry.Rectangle2D primScreenBounds = Screen.getPrimary().getVisualBounds();
        stage.setX((primScreenBounds.getWidth() - stage.getWidth()) / 2);
        stage.setY((primScreenBounds.getHeight() - stage.getHeight()) / 4);
    }

    @FXML
    public void novaTurma(ActionEvent e) throws IOException {

        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/fxml/CriarTurma.fxml"));
        Parent root = loader.load();

        CriarTurmaController controller = loader.getController();
        controller.dadosInicio(user);
        Scene scene = new Scene(root);
        scene.getStylesheets().add("/styles/Styles.css");

        Stage stage = (Stage) ((Node) e.getSource()).getScene().getWindow();
        stage.setTitle("Gestão de Escola de Formação");
        stage.setScene(scene);
        stage.show();

        javafx.geometry.Rectangle2D primScreenBounds = Screen.getPrimary().getVisualBounds();
        stage.setX((primScreenBounds.getWidth() - stage.getWidth()) / 2);
        stage.setY((primScreenBounds.getHeight() - stage.getHeight()) / 4);
    }

    @FXML
    public void listarTurmas(ActionEvent e) throws IOException {
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/fxml/ListarTurmas.fxml"));
        Parent root = loader.load();

        ListarTurmasController controller = loader.getController();
        controller.dadosInicio(user);
        Scene scene = new Scene(root);
        scene.getStylesheets().add("/styles/Styles.css");

        Stage stage = (Stage) ((Node) e.getSource()).getScene().getWindow();
        stage.setTitle("Gestão de Escola de Formação");
        stage.setScene(scene);
        stage.show();

        javafx.geometry.Rectangle2D primScreenBounds = Screen.getPrimary().getVisualBounds();
        stage.setX((primScreenBounds.getWidth() - stage.getWidth()) / 2);
        stage.setY((primScreenBounds.getHeight() - stage.getHeight()) / 4);
    }

    @FXML
    public void novaDisciplina(ActionEvent e) throws IOException {
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/fxml/NovaDisciplina.fxml"));
        Parent root = loader.load();

        NovaDisciplinaController controller = loader.getController();
        controller.dadosInicio(user);
        Scene scene = new Scene(root);
        scene.getStylesheets().add("/styles/Styles.css");

        Stage stage = (Stage) ((Node) e.getSource()).getScene().getWindow();
        stage.setTitle("Gestão de Escola de Formação");
        stage.setScene(scene);
        stage.show();

        javafx.geometry.Rectangle2D primScreenBounds = Screen.getPrimary().getVisualBounds();
        stage.setX((primScreenBounds.getWidth() - stage.getWidth()) / 2);
        stage.setY((primScreenBounds.getHeight() - stage.getHeight()) / 4);
    }

    @FXML
    public void listarDisciplinas(ActionEvent e) throws IOException {
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/fxml/ListarDisciplinas.fxml"));
        Parent root = loader.load();

        ListarDisciplinasController controller = loader.getController();
        controller.dadosInicio(user);
        Scene scene = new Scene(root);
        scene.getStylesheets().add("/styles/Styles.css");

        Stage stage = (Stage) ((Node) e.getSource()).getScene().getWindow();
        stage.setTitle("Gestão de Escola de Formação");
        stage.setScene(scene);

        stage.show();

        stage.show();

        javafx.geometry.Rectangle2D primScreenBounds = Screen.getPrimary().getVisualBounds();
        stage.setX((primScreenBounds.getWidth() - stage.getWidth()) / 2);
        stage.setY((primScreenBounds.getHeight() - stage.getHeight()) / 4);
    }

    @FXML
    public void novoFormador(ActionEvent e) throws IOException {

        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/fxml/NovoFormador.fxml"));
        Parent root = loader.load();

        NovoFormadorController controller = loader.getController();
        controller.dadosInicio(user);
        Scene scene = new Scene(root);
        scene.getStylesheets().add("/styles/Styles.css");

        Stage stage = (Stage) ((Node) e.getSource()).getScene().getWindow();
        stage.setTitle("Gestão de Escola de Formação");
        stage.setScene(scene);

        stage.show();

        javafx.geometry.Rectangle2D primScreenBounds = Screen.getPrimary().getVisualBounds();
        stage.setX((primScreenBounds.getWidth() - stage.getWidth()) / 2);
        stage.setY((primScreenBounds.getHeight() - stage.getHeight()) / 4);
    }

    @FXML
    public void listarFormadores(ActionEvent e) throws IOException {
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/fxml/ListarFormadores.fxml"));
        Parent root = loader.load();

        ListarFormadoresController controller = loader.getController();
        controller.dadosInicio(user);
        Scene scene = new Scene(root);
        scene.getStylesheets().add("/styles/Styles.css");

        Stage stage = (Stage) ((Node) e.getSource()).getScene().getWindow();
        stage.setTitle("Gestão de Escola de Formação");
        stage.setScene(scene);

        stage.show();

        stage.show();

        javafx.geometry.Rectangle2D primScreenBounds = Screen.getPrimary().getVisualBounds();
        stage.setX((primScreenBounds.getWidth() - stage.getWidth()) / 2);
        stage.setY((primScreenBounds.getHeight() - stage.getHeight()) / 4);
    }

    @FXML
    public void listarSumarios(ActionEvent e) throws IOException {
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/fxml/ListarSumarios.fxml"));
        Parent root = loader.load();

        ListarSumariosController controller = loader.getController();
        controller.dadosInicio(user);
        Scene scene = new Scene(root);
        scene.getStylesheets().add("/styles/Styles.css");

        Stage stage = (Stage) ((Node) e.getSource()).getScene().getWindow();
        stage.setTitle("Gestão de Escola de Formação");
        stage.setScene(scene);

        stage.show();

        stage.show();

        javafx.geometry.Rectangle2D primScreenBounds = Screen.getPrimary().getVisualBounds();
        stage.setX((primScreenBounds.getWidth() - stage.getWidth()) / 2);
        stage.setY((primScreenBounds.getHeight() - stage.getHeight()) / 4);
    }
    
    public void initData(Utilizador utilizador) {
        user = utilizador;

        if (user == null) {
            return;
        }
        lblNomeUtilizador.setText(user.getNomeUtilizador());
    }

    @FXML
    private void terminarSessao(ActionEvent event) {
        String resultado = Alertas.alertaConfirmacao("Confirmação - Terminar Sessão", "Tem a certeza que pretende terminar sessão?").getResult().getText();

        if (resultado.equalsIgnoreCase("OK")) {
            System.exit(0);
        }
    }

}
