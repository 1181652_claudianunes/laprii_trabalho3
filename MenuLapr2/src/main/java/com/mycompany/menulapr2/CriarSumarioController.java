/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.menulapr2;

import java.io.IOException;
import static java.lang.Integer.parseInt;
import java.net.URL;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDate;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.text.Text;
import javafx.stage.Stage;

/**
 *
 * @author Renato
 */
public class CriarSumarioController {

    @FXML
    private Text lblCriacaoSumario;

    @FXML
    private Text lblCurso;

    @FXML
    private ComboBox cbMostrarCurso;

    @FXML
    private Text lblFormador;

    @FXML
    private ComboBox cbMostrarDisciplina;

    @FXML
    private Text lblDisciplina;

    @FXML
    private ComboBox cbMostrarFormador;

    @FXML
    private Text lblData;

    @FXML
    private DatePicker dateData;

    @FXML
    private Text lblHora;

    @FXML
    private TextField txtfieldhorainicio;

    @FXML
    private Text lblAte;

    @FXML
    private TextField txtfieldhorafim;

    @FXML
    private Button buttonVoltar;

    @FXML
    private Button buttonConfirmar;

    @FXML
    private Button buttonApagar;

    @FXML
    private Text lblSumario;

    @FXML
    private TextArea txtareaSumario;

    private Utilizador utilizadorAtual;

    public void initialize(URL url, ResourceBundle rb) {
        propriedadesIniciais();
        
        dateData.valueProperty().addListener((data, dataInicial, novaData) -> {

            if (dateData.valueProperty().isNull().get() == false && dateData.valueProperty().getValue().compareTo(LocalDate.now()) < 0) {

                Alertas.alertaErro("Erro - Data nao pode ser mais antiga que a data do sistema", "Introduzir data valida");

                dateData.setValue(null);

            };
        });
        
         txtfieldhorainicio.textProperty().addListener((horainicion, horaantiga, horafim) -> {

            if (txtfieldhorainicio.getText().isEmpty() == false && parseInt(txtfieldhorainicio.getText()) > 23) {

                txtfieldhorainicio.deleteNextChar();

                Alertas.alertaErro("Erro - Numero de horario", "A hora ");

            };

        });

    }

    public void propriedadesIniciais() {

        ObservableList<String> listaCursos = FXCollections.observableArrayList();

        ObservableList<String> listaDisciplina = FXCollections.observableArrayList();

        ObservableList<String> listaFormador = FXCollections.observableArrayList();

        try {

            Statement query = Conexao.connectarBD().createStatement();

            ResultSet rs = query.executeQuery("select distinct nome from Curso order by nome asc");

            while (rs.next()) {

                String cursoTemp = rs.getString("nome");

                listaCursos.add(cursoTemp);

            }

            cbMostrarCurso.setItems(listaCursos);

        } catch (SQLException e) {

            System.out.println(e.getMessage());

        }

        try {

            Statement query = Conexao.connectarBD().createStatement();

            ResultSet rs = query.executeQuery("select distinct disciplina from Disciplina order by nome asc");

            while (rs.next()) {

                String disciplinaTemp = rs.getString("disciplina");

                listaDisciplina.add(disciplinaTemp);

            }

            cbMostrarDisciplina.setItems(listaDisciplina);

        } catch (SQLException e) {

            System.out.println(e.getMessage());

        }

        try {

            Statement query = Conexao.connectarBD().createStatement();

            ResultSet rs = query.executeQuery("select distinct formador from Formador order by nome asc");

            while (rs.next()) {

                String formadorTemp = rs.getString("formador");

                listaFormador.add(formadorTemp);

            }

            cbMostrarFormador.setItems(listaFormador);

        } catch (SQLException e) {

            System.out.println(e.getMessage());

        }

    }

    public void dadosInicio(Utilizador utilizador) {
        utilizadorAtual = utilizador;
    }

    @FXML

    public void goToAdministracao(ActionEvent e) throws IOException {

        if (Alertas.alertaConfirmacao("Confirmação", "Deseja voltar para a zona de Administração? Se escolher OK, todos os dados que não foram gravados vão ser apagados").getResult() == ButtonType.OK) {

            FXMLLoader loader = new FXMLLoader();

            loader.setLocation(getClass().getResource("/fxml/Administracao.fxml"));

            AdministracaoController controller = loader.getController();
            controller.initData(utilizadorAtual); //passar o objeto Utilizador para a próxima Scene;

            Parent root = loader.load();

            Scene scene = new Scene(root);

            scene.getStylesheets().add("/styles/Styles.css");

            Stage stage = (Stage) ((Node) e.getSource()).getScene().getWindow();

            stage.setTitle("Sistema de Gestão de Hoteis");

            stage.setScene(scene);

            stage.show();

        }

    }

    @FXML

    public void apagarDados(ActionEvent e) {

        if (Alertas.alertaConfirmacao("Confirmação - Apagar Dados", "Clique em OK para apagar todos os dados").getResult() == ButtonType.OK) {

            cbMostrarDisciplina.getItems().clear();

            cbMostrarFormador.getItems().clear();

            txtareaSumario.setText("");

            txtfieldhorafim.setText("");

            txtfieldhorainicio.setText("");

            dateData.setValue(null);

            cbMostrarCurso.getItems().clear();

            ObservableList<String> listaCursos = FXCollections.observableArrayList();

            try {

                Statement query = Conexao.connectarBD().createStatement();

                ResultSet rs = query.executeQuery("select distinct nome from Curso order by nome asc");

                while (rs.next()) {

                    String cursoTemp = rs.getString("nome");

                    listaCursos.add(cursoTemp);

                }

                cbMostrarCurso.setItems(listaCursos);

            } catch (SQLException h) {

                System.out.println(h.getMessage());

            }

            cbMostrarCurso.setItems(listaCursos);

        }

    }

}
