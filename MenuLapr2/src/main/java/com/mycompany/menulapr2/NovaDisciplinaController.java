package com.mycompany.menulapr2;

/*
  * To change this license header, choose License Headers in Project Properties.
  * To change this template file, choose Tools | Templates
  * and open the template in the editor.
 */
import de.jensd.fx.glyphs.materialdesignicons.MaterialDesignIconView;
import java.io.IOException;
import static java.lang.Double.parseDouble;
import static java.lang.Integer.parseInt;
import java.net.URL;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.Normalizer;
import java.text.Normalizer.Form;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author Goldb
 */
public class NovaDisciplinaController implements Initializable {

    @FXML
    private Button buttonConfirmar;
    @FXML
    private MaterialDesignIconView iconConfirmar;
    @FXML
    private Button buttonReset;
    @FXML
    private MaterialDesignIconView iconReset;
    @FXML
    private MaterialDesignIconView iconVoltar;
    @FXML
    private Button buttonVoltarAtras;
    @FXML
    private Label lblNomeDisciplina;
    @FXML
    private TextField txtNomeDisciplina;
    @FXML
    private Label lblDescricao;
    @FXML
    private TextArea txtAreaDescricao;
    
    private Utilizador utilizadorAtual;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {

        txtNomeDisciplina.textProperty().addListener((valorDuracao, valorAntigo, novoValor) -> {
            if (!txtNomeDisciplina.getText().matches("a-z")) {

                txtNomeDisciplina.deleteNextChar();
            }

            if (txtNomeDisciplina.getText().isEmpty() == false && txtNomeDisciplina.getText().length() > 50) {
                txtNomeDisciplina.deleteNextChar();
                Alertas.alertaErro("Erro -Número de caracteres", "O número de caracteres máximos neste campo é de 50");
            }

        });

        txtAreaDescricao.textProperty().addListener((valorDuracao, valorAntigo, novoValor) -> {

            if (!txtNomeDisciplina.getText().matches("a-z")) {

                txtNomeDisciplina.deleteNextChar();

            }

            if (txtAreaDescricao.getText().isEmpty() == false && txtAreaDescricao.getText().length() > 100) {
                txtAreaDescricao.deleteNextChar();
                Alertas.alertaErro("Erro -Número de caracteres", "O número de caracteres máximos neste campo é de 100");
            }
        });
    }
    
    public void dadosInicio(Utilizador utilizador) {
        utilizadorAtual = utilizador;
    }

    public void prevenirNaoLetras(String texto) {

        for (int i = 0; i < texto.length(); i++) {
            char caracter = texto.charAt(i);

            if (Character.isLetter(i) == false) {
                texto.replace(caracter, ' ');
            }
        }
    }

    @FXML
    public void goToAdministracao(ActionEvent e) throws IOException {

        if (Alertas.alertaConfirmacao("Confirmação", "Deseja voltar para a zona de Administração? Se escolher OK, todos os dados que não foram gravados vão ser apagados").getResult() == ButtonType.OK) {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("/fxml/Administracao.fxml"));
            Parent root = loader.load();
            
            AdministracaoController controller = loader.getController();
            controller.initData(utilizadorAtual); //passar o objeto Utilizador para a próxima Scene;

            Scene scene = new Scene(root);
            scene.getStylesheets().add("/styles/Styles.css");

            Stage stage = (Stage) ((Node) e.getSource()).getScene().getWindow();
            stage.setTitle("Sistema de Gestão de Hoteis");
            stage.setScene(scene);
            stage.show();
        }
    }

    @FXML
    public void apagarDados(ActionEvent e) {

        if (Alertas.alertaConfirmacao("Confirmação - Apagar Dados", "Clique em OK para apagar todos os dados").getResult() == ButtonType.OK) {

            txtNomeDisciplina.setText("");
            txtAreaDescricao.setText("");

        }
    }

    /**
     * Método para o campo [NOME] e [ÁREA DE ENSINO] Evento do tipo KeyEvent
     * para garantir que no campo Nome, só são introduzidos as letras
     * convencionais para nomes
     */
    @FXML
    public void integridadeCampoLirico1(javafx.scene.input.KeyEvent event) {
        String caracterEscrito = event.getCharacter();
        for (int i = 0; i < caracterEscrito.length(); i++) {
            Character c = caracterEscrito.charAt(i);

            if (Character.isDigit(c) || !Character.isLetter(c)) { //se o caracter introduzido for um número ou não for uma letra, este será "consumido"
                if (Character.isSpaceChar(c)) {
                    /*
                     o primeiro ciclo testa se o caracter introduzido é um digito e se não é uma letra, ou seja, testa
                     se é um cáracter tipo: @ - , . / ,etc. e também testa se é um espaço em branco, no entanto, se for
                     um espaço branco, têm que se abrir uma exceção pois o campo Nome , recebe o nome completo do cliente,
                     o que indica que vai ser preciso espaços. Para isso, criamos um segundo ciclo if, para estar se é um
                     espaço, se for um espaço, abre uma exceção e deixa o funcionário introduzir, senão, passa para o ciclo
                     else em baixo e "consome" todos as "não-letras"
                     */
                } else {
                    event.consume();
                }
            }
        }
    }

    @FXML
    public void registarDisciplina(ActionEvent e) {

        if (txtNomeDisciplina.getText().isEmpty() == true) {
            Alertas.alertaErro("Erro - Preenchimento obrigatório", "É obrigatório preencher o nome da disciplina para efetuar registos");
            //break updateBaseDeDados;

        } else if (txtNomeDisciplina.getText().isEmpty() == false && verificarDisciplina(txtNomeDisciplina.getText()) == true) {
            Alertas.alertaConfirmacao("Erro - Disciplina Duplicada", "A disciplina introduzida já existe. Introduza novamente");
            txtNomeDisciplina.deletePreviousChar();
        } else {

            try {

                String query = "INSERT INTO Disciplina(nome,descricao) "
                        + "VALUES ('" + txtNomeDisciplina.getText() + "','" + txtAreaDescricao.getText() + "')";

                Statement queryBD = Conexao.connectarBD().createStatement();
                queryBD.executeUpdate(query);

                Alertas.alertaInformacao("Registo com sucesso", "Todos os dados foram registados na base de dados com sucesso");

                txtNomeDisciplina.setText("");
                txtAreaDescricao.setText("");

            } catch (SQLException exception) {
                System.out.println(exception.getMessage());
            }
        }
    }

    public boolean verificarDisciplina(String nomeDisciplina) {
        boolean disciplinaExiste = false;

        try {
            Statement query = Conexao.connectarBD().createStatement();
            ResultSet rs = query.executeQuery("SELECT nome FROM Disciplina");

            while (rs.next()) {
                if (rs.getString("nome").equalsIgnoreCase(removerAcentos(nomeDisciplina))) {
                    disciplinaExiste = true;
                    break;
                }
            }
        } catch (SQLException v) {
            System.out.println(v.getMessage());
        }

        return disciplinaExiste;
    }

    public static String removerAcentos(String texto) {
        return texto == null ? null : Normalizer.normalize(texto, Form.NFD)
                .replaceAll("\\p{InCombiningDiacriticalMarks}+", "");
    }

}
