/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.menulapr2;

/**
 *
 * @author Goldb
 */
public class Curso {
    private String areaEnsino;
    private int cod_curso;
    private int duracaoCurso;
    private String nome;
    private int numeroVagas;
    private double preco;
    private String regime;
    private int vagasMinimas;

    public Curso(String areaEnsino, int duracaoCurso, String nome, int numeroVagas, double preco, String regime) {
        this.areaEnsino = areaEnsino;
        this.duracaoCurso = duracaoCurso;
        this.nome = nome;
        this.numeroVagas = numeroVagas;
        this.preco = preco;
        this.regime = regime;
    }

    public String getAreaEnsino() {
        return areaEnsino;
    }

    public void setAreaEnsino(String areaEnsino) {
        this.areaEnsino = areaEnsino;
    }

    public int getCod_curso() {
        return cod_curso;
    }

    public void setCod_curso(int cod_curso) {
        this.cod_curso = cod_curso;
    }

    public int getDuracaoCurso() {
        return duracaoCurso;
    }

    public void setDuracaoCurso(int duracaoCurso) {
        this.duracaoCurso = duracaoCurso;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public int getNumeroVagas() {
        return numeroVagas;
    }

    public void setNumeroVagas(int numeroVagas) {
        this.numeroVagas = numeroVagas;
    }

    public double getPreco() {
        return preco;
    }

    public void setPreco(double preco) {
        this.preco = preco;
    }

    public String getRegime() {
        return regime;
    }

    public void setRegime(String regime) {
        this.regime = regime;
    }

    public int getVagasMinimas() {
        return vagasMinimas;
    }

    public void setVagasMinimas(int vagasMinimas) {
        this.vagasMinimas = vagasMinimas;
    }
    
    
}
