package com.mycompany.menulapr2;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Goldb
 */
public class Conexao {
    
    private static final String DEFAULT_URL = "jdbc:sqlserver://193.136.62.208;databaseName=lapr_grupo3";
    private static final String DEFAULT_USERNAME = "grupo3";
    private static final String DEFAULT_PASSWORD = "TEsP@D1AS";
    
    public static Connection connectarBD() {
        Connection con = null;
        
        try {
            con  = DriverManager.getConnection(DEFAULT_URL,DEFAULT_USERNAME,DEFAULT_PASSWORD);

            if (con != null) {
                DatabaseMetaData dm = (DatabaseMetaData) con.getMetaData();
                System.out.println("Driver name: " + dm.getDriverName());
                System.out.println("Driver version: " + dm.getDriverVersion());
                System.out.println("Product name: " + dm.getDatabaseProductName());
                System.out.println("Product version: " + dm.getDatabaseProductVersion());
            }
            
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        } 
        
        return con;
    }
    
    public static void fecharConexao(Connection connect) throws SQLException{
        connect.close();
    }
}
