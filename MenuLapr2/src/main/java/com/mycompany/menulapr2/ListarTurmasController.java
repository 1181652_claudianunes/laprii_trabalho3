package com.mycompany.menulapr2;

/*
  * To change this license header, choose License Headers in Project Properties.
  * To change this template file, choose Tools | Templates
  * and open the template in the editor.
 */
import com.mycompany.menulapr2.Alertas;
import com.mycompany.menulapr2.Conexao;
import com.mycompany.menulapr2.Turma;
import de.jensd.fx.glyphs.materialdesignicons.MaterialDesignIconView;
import java.io.IOException;
import java.net.URL;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TableView;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author Goldb
 */
public class ListarTurmasController implements Initializable {

    @FXML
    private TableView<Turma> tblViewTurma;
    @FXML
    private Button buttonVoltarAtras;
    @FXML
    private MaterialDesignIconView iconVoltarAtras;
    
    private Utilizador utilizadorAtual;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        preencherView();
    }
    
    public void dadosInicio(Utilizador utilizador) {
        utilizadorAtual = utilizador;
    }

    public void preencherView() {
        ObservableList<Turma> listaTurmas = FXCollections.observableArrayList();
        try {

            Statement query = Conexao.connectarBD().createStatement();
            ResultSet resultados = query.executeQuery("SELECT * FROM turmaCurso GROUP BY cod_curso,nome,nomeTurma,data_inicio,data_fim");
            while (resultados.next()) {
                Turma turma = new Turma(resultados.getString("nome"), resultados.getString("data_inicio"), resultados.getString("data_fim"), resultados.getString("nomeTurma"));
                listaTurmas.add(turma);
            }

        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }

        tblViewTurma.setItems(listaTurmas);
    }

    @FXML
    public void voltarAtras(ActionEvent e) throws IOException {
        if (Alertas.alertaConfirmacao("Voltar para página anterior", "Se clica em OK irá voltar para a página anterior").getResult() == ButtonType.OK) {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("/fxml/Administracao.fxml"));
            Parent root = loader.load();
            
            AdministracaoController controller = loader.getController();
            controller.initData(utilizadorAtual); //passar o objeto Utilizador para a próxima Scene;

            Scene scene = new Scene(root);
            scene.getStylesheets().add("/styles/Styles.css");

            Stage stage = (Stage) ((Node) e.getSource()).getScene().getWindow();
            stage.setTitle("Gestão de Escola de Formação");
            stage.setScene(scene);
            stage.show();
        }
    }
}
