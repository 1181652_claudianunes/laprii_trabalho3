/*

 * To change this license header, choose License Headers in Project Properties.

 * To change this template file, choose Tools | Templates

 * and open the template in the editor.

 */
package com.mycompany.menulapr2;

import de.jensd.fx.glyphs.materialdesignicons.MaterialDesignIconView;
import java.io.IOException;
import static java.lang.Integer.parseInt;
import java.lang.invoke.MethodHandles;
import java.net.URL;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDate;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

/**
 *
 * FXML Controller class
 *
 *
 *
 * @author Cláudia Nunes
 *
 */
public class CriarTurmaController implements Initializable {

    private String nome_turma;

    @FXML
    private Label lblNomeCurso;
    @FXML
    private TextField txtfieldNumeroTurmas;
    @FXML
    private TextField txtfieldNumeroAlunosTurma;
    @FXML
    private ComboBox cbMostrarCursos;
    @FXML
    private DatePicker dateDataInicial;
    @FXML
    private DatePicker dateDataFim;
    @FXML
    private Button buttonConfirmar;
    @FXML
    private MaterialDesignIconView iconConfirmar;
    @FXML
    private Button buttonReset;
    @FXML
    private MaterialDesignIconView iconReset;
    @FXML
    private Button buttonVoltarAtras;
    @FXML
    private MaterialDesignIconView iconVoltar;
    @FXML
    private Label lblNumeroAlunosInscritos;
    @FXML
    private Label lblNumeroTurmas;
    @FXML
    private Label lblDataFim;
    @FXML
    private Label lblDataInicio;

    
    private Utilizador utilizadorAtual;

    /**
     *
     * Initializes the controller class.
     *
     */
    @Override

    public void initialize(URL url, ResourceBundle rb) {

        propriedadesIniciais();
        dateDataFim.valueProperty().addListener((dataFim, dataInicial, novaData) -> {

            if (dateDataFim.valueProperty().isNull().get() == false && dateDataInicial.valueProperty().isNull().get() == false && dateDataFim.valueProperty().getValue().compareTo(dateDataInicial.valueProperty().getValue()) == 0) {
                Alertas.alertaErro("Erro - Data final nao pode ser igual a data inicial", "Inserir datas diferentes");

                dateDataFim.setValue(null);
                dateDataInicial.setValue(null);
            };

            if (dateDataFim.valueProperty().isNull().get() == false && dateDataInicial.valueProperty().isNull().get() == false && dateDataFim.valueProperty().getValue().compareTo(dateDataInicial.valueProperty().getValue()) < 0) {

                Alertas.alertaErro("Erro - Data final nao pode ser mais antiga que a data inicial", "Introduzir datas validas");

                dateDataFim.setValue(null);
                dateDataInicial.setValue(null);
            };

            if (dateDataInicial.valueProperty().isNull().get() == false && dateDataFim.valueProperty().isNull().get() == false && dateDataInicial.valueProperty().getValue().compareTo(LocalDate.now()) < 0) {

                Alertas.alertaErro("Erro - Data final nao pode ser mais antiga que a data do sistema", "Introduzir datas validas");

                dateDataFim.setValue(null);
                dateDataInicial.setValue(null);
            };

            if (dateDataFim.valueProperty().isNull().get() == false && dateDataInicial.valueProperty().isNull().get() == false && dateDataFim.valueProperty().getValue().compareTo(LocalDate.now()) == 0) {

                Alertas.alertaErro("Erro - Data final nao pode ser igual a do sistema", "Introduzir data valida");
                dateDataFim.setValue(null);
                dateDataInicial.setValue(null);
            }

        });

        dateDataInicial.valueProperty().addListener((dataInicial, primeiraData, novaData) -> {

            if (dateDataFim.valueProperty().isNull().get() == false && dateDataInicial.valueProperty().isNull().get() == false && dateDataFim.valueProperty().getValue().compareTo(dateDataInicial.valueProperty().getValue()) < 0) {

                Alertas.alertaErro("Erro - Data final nao pode ser mais antiga que a data inicial", "Introduzir datas validas");
                dateDataFim.setValue(null);
                dateDataInicial.setValue(null);
            };

            if (dateDataInicial.valueProperty().isNull().get() == false && dateDataFim.valueProperty().isNull().get() == false && dateDataInicial.valueProperty().getValue().compareTo(LocalDate.now()) < 0) {

                Alertas.alertaErro("Erro - Data inicial nao pode ser mais antiga que a data do sistema", "Introduzir datas validas");

                dateDataFim.setValue(null);
                dateDataInicial.setValue(null);
            };

            if (dateDataFim.valueProperty().isNull().get() == false && dateDataInicial.valueProperty().isNull().get() == false && dateDataFim.valueProperty().getValue().compareTo(dateDataInicial.valueProperty().getValue()) == 0) {
                Alertas.alertaErro("Erro - Data final nao pode ser igual a data inicial", "Inserir datas diferentes");

                dateDataFim.setValue(null);
                dateDataInicial.setValue(null);
            };

            if (dateDataFim.valueProperty().isNull().get() == false && dateDataInicial.valueProperty().isNull().get() == false && dateDataFim.valueProperty().getValue().compareTo(LocalDate.now()) == 0) {

                Alertas.alertaErro("Erro - Data final nao pode ser igual a do sistema", "Introduzir datas validas");
                dateDataFim.setValue(null);
                dateDataInicial.setValue(null);
            }

        });

        txtfieldNumeroTurmas.textProperty().addListener((numeroTurmas, numeroAntigo, numeroNovo) -> {

            if (txtfieldNumeroTurmas.getText().isEmpty() == false && parseInt(txtfieldNumeroTurmas.getText()) > 4) {

                txtfieldNumeroTurmas.deleteNextChar();

                Alertas.alertaErro("Erro - Numero de turmas", "O numero maximo de turmas e 4");

            };

        });

        txtfieldNumeroTurmas.textProperty().addListener((numeroTurmas, numeroAntigo, numeroNovo) -> {

            if (txtfieldNumeroTurmas.getText().isEmpty() == false && parseInt(txtfieldNumeroTurmas.getText()) < 1) {

                txtfieldNumeroTurmas.deleteNextChar();

                Alertas.alertaErro("Erro - Numero de turmas", "O numero minimo de turmas e 1");

            };

        });

        txtfieldNumeroAlunosTurma.textProperty().addListener((numeroAlunosTurma, numeroAntigo, numeroNovo) -> {

            if (txtfieldNumeroAlunosTurma.getText().isEmpty() == false && parseInt(txtfieldNumeroAlunosTurma.getText()) > 40) {

                txtfieldNumeroAlunosTurma.deleteNextChar();

                Alertas.alertaErro("Erro - Numero de alunos por turma", "O numero maximo de alunos por turma e 40");

            };

        });

    }

    public void dadosInicio(Utilizador utilizador) {
        utilizadorAtual = utilizador;
    }

    public void propriedadesIniciais() {

        ObservableList<String> listaCursos = FXCollections.observableArrayList();

        try {

            Statement query = Conexao.connectarBD().createStatement();

            ResultSet rs = query.executeQuery("select distinct nome from Curso order by nome asc");

            while (rs.next()) {

                String cursoTemp = rs.getString("nome");

                listaCursos.add(cursoTemp);

            }

            cbMostrarCursos.setItems(listaCursos);

        } catch (SQLException e) {

            System.out.println(e.getMessage());

        }

    }

    @FXML

    public void goToAdministracao(ActionEvent e) throws IOException {

        if (Alertas.alertaConfirmacao("Confirmação", "Deseja voltar para a zona de Administração? Se escolher OK, todos os dados que não foram gravados vão ser apagados").getResult() == ButtonType.OK) {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("/fxml/Administracao.fxml"));
            Parent root = loader.load();
            
            AdministracaoController controller = loader.getController();
            controller.initData(utilizadorAtual); //passar o objeto utilizador para a próxima Scene;

            Scene scene = new Scene(root);
            scene.getStylesheets().add("/styles/Styles.css");

            Stage stage = (Stage) ((Node) e.getSource()).getScene().getWindow();
            stage.setTitle("Sistema de Gestão de Hoteis");
            stage.setScene(scene);
            stage.show();
        }

    }

    @FXML

    public void apagarDados(ActionEvent e) {

        if (Alertas.alertaConfirmacao("Confirmação - Apagar Dados", "Clique em OK para apagar todos os dados").getResult() == ButtonType.OK) {

            txtfieldNumeroTurmas.setText("");

            txtfieldNumeroAlunosTurma.setText("");

            dateDataInicial.setValue(null);

            dateDataFim.setValue(null);

            cbMostrarCursos.getItems().clear();

            ObservableList<String> listaCursos = FXCollections.observableArrayList();

            try {

                Statement query = Conexao.connectarBD().createStatement();

                ResultSet rs = query.executeQuery("select distinct nome from Curso order by nome asc");

                while (rs.next()) {

                    String cursoTemp = rs.getString("nome");

                    listaCursos.add(cursoTemp);

                }

                cbMostrarCursos.setItems(listaCursos);

            } catch (SQLException h) {

                System.out.println(h.getMessage());

            }

            cbMostrarCursos.setItems(listaCursos);

        }

    }

    @FXML

    public void integridadeCampoNumerico1(javafx.scene.input.KeyEvent event) {

        String caracterEscrito = event.getCharacter();

        for (int i = 0; i < caracterEscrito.length(); i++) {

            Character c = caracterEscrito.charAt(i);

            if (txtfieldNumeroTurmas.textProperty().isEmpty().getValue() == false) { // se a textField não estiver vazia, é permitido introduzir o valor 0, para números como: 10,20,etc.

                if (!Character.isDigit(c)) {

                    event.consume();

                }

            } else { //se a textField estiver vazia, não é permitido introduzir o valor 0, pois o minimo número de pessoas é 1

                if (!Character.isDigit(c) || c == '0') {

                    event.consume();

                }

            }

        }

    }

    @FXML

    public void integridadeCampoNumerico2(javafx.scene.input.KeyEvent event) {

        String caracterEscrito = event.getCharacter();

        for (int i = 0; i < caracterEscrito.length(); i++) {

            Character c = caracterEscrito.charAt(i);

            if (txtfieldNumeroAlunosTurma.textProperty().isEmpty().getValue() == false) { // se a textField não estiver vazia, é permitido introduzir o valor 0, para números como: 10,20,etc.

                if (!Character.isDigit(c)) {

                    event.consume();

                }

            } else { //se a textField estiver vazia, não é permitido introduzir o valor 0, pois o minimo número de pessoas é 1

                if (!Character.isDigit(c) || c == '0') {

                    event.consume();

                }

            }

        }

    }

    @FXML

    public void registarTurma(ActionEvent e) {

        try {

            if (txtfieldNumeroTurmas.getText().isEmpty() == false) {

                Double.parseDouble(txtfieldNumeroTurmas.getText());

                //break updateBaseDeDados;
            }

        } catch (NumberFormatException ex) {

            Alertas.alertaErro("Erro - Numero minimo de turmas", "Tem de existir pelo menos uma turma.");

        }

        if (txtfieldNumeroAlunosTurma.getText().isEmpty() == false && parseInt(txtfieldNumeroAlunosTurma.getText()) < 10) {

            Alertas.alertaErro("Erro - Número minimo de alunos", "As turmas tem que ter no minimo 10 alunos por turma.");

            //break updateBaseDeDados;        
        }

        if (txtfieldNumeroTurmas.getText().isEmpty() == true || txtfieldNumeroAlunosTurma.getText().isEmpty() == true || cbMostrarCursos.valueProperty().isNull().getValue() == true || dateDataInicial.valueProperty().isNull().getValue() == true || dateDataFim.valueProperty().isNull().getValue() == true) {

            Alertas.alertaErro("Erro - Campos vazios", "Todos os campos têm que ser preenchidos para fazer um registo");

            //break updateBaseDeDados;
            if (txtfieldNumeroAlunosTurma.getText().isEmpty() == false && parseInt(txtfieldNumeroAlunosTurma.getText()) > 40) {

                Alertas.alertaErro("Erro - Numero maximo de alunos", "As turmas so podem ter no maximo 40 alunos por turma.");

                //break updateBaseDeDados;        
            }

            if (txtfieldNumeroTurmas.getText().isEmpty() == false && parseInt(txtfieldNumeroTurmas.getText()) > 4) {

                Alertas.alertaErro("Erro - Numero maximo de turmas", "So pode haver no maximo 4 turmas.");

                //break updateBaseDeDados;        
            }

            if (txtfieldNumeroAlunosTurma.getText().isEmpty() && parseInt(txtfieldNumeroAlunosTurma.getText()) < 10) {

                Alertas.alertaErro("Erro - Numero minimo de alunos por turmas", "Tem que haver no minimo 10 alunos por turma");

            }

        } else {

            try {

                Statement query = Conexao.connectarBD().createStatement();

                ResultSet rs = query.executeQuery("SELECT * FROM Turma WHERE cod_curso = " + getCodCurso(cbMostrarCursos.valueProperty().get().toString()));

                String next = "";

                String nomeTurma = "";

                while (rs.next()) {
                    nomeTurma = rs.getString("nomeTurma");
                }

                if (nomeTurma.isEmpty() == false) {

                    int charValue = nomeTurma.charAt(0);
                    next = String.valueOf((char) (charValue + 1));
                } else {
                    next = "A";
                }
                Conexao.connectarBD().close();
                String insert = "INSERT INTO Turma(cod_curso,data_inicio,data_fim,nomeTurma) VALUES (" + getCodCurso(cbMostrarCursos.valueProperty().get().toString()) + ",'" + dateDataInicial.valueProperty().asString().get() + "','" + dateDataFim.valueProperty().asString().get() + "','" + next + "')";

                query = Conexao.connectarBD().createStatement();

                query.executeUpdate(insert);

            } catch (SQLException exception) {

                System.out.println(exception.getMessage());
            }

        }

    }

    public int getCodCurso(String nomeCurso) {

        int codCurso = 0;

        try {

            Statement query = Conexao.connectarBD().createStatement();

            ResultSet rs = query.executeQuery("SELECT cod_curso FROM Curso WHERE nome LIKE '" + nomeCurso + "'");

            while (rs.next()) {

                codCurso = rs.getInt("cod_curso");

            }
            System.out.println("CODCURSO :" + codCurso);

        } catch (SQLException e) {

            System.out.println(e.getMessage());

        }

        return codCurso;

    }

}
