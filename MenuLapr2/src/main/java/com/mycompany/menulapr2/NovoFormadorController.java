/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.menulapr2;

import de.jensd.fx.glyphs.materialdesignicons.MaterialDesignIconView;
import java.io.IOException;
import static java.lang.Double.parseDouble;
import static java.lang.Integer.parseInt;
import java.net.URL;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyEvent;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author Cláudia Nunes
 */
public class NovoFormadorController implements Initializable {

    @FXML
    private TextField txtNif;
    @FXML
    private TextField txtNomeCompleto;
    @FXML
    private TextField txtRua;
    @FXML
    private TextField txtCodPostal;
    @FXML
    private TextField txtContactoTelefonico;
    @FXML
    private Button buttonConfirmar;
    @FXML
    private MaterialDesignIconView iconConfirmar;
    @FXML
    private Button buttonReset;
    @FXML
    private MaterialDesignIconView iconReset;
    @FXML
    private Button buttonVoltarAtras;
    @FXML
    private MaterialDesignIconView iconVoltar;
    @FXML
    private TableView<Disciplinas> tblViewDisciplina;
    @FXML
    private TableColumn<Disciplinas, Integer> colunaCodigo;
    @FXML
    private TableColumn<Disciplinas, String> colunaNome;
    @FXML
    private Label lblNif;
    @FXML
    private Label lblNomeCompleto;
    @FXML
    private Label lblRua;
    @FXML
    private Label lblCodPostal;
    @FXML
    private Label lblContactoTelefonico;
    @FXML
    private Label lblDisciplina;
    @FXML
    private TextField txtDisciplina;
    @FXML
    private Label lblListaDisciplinas;
    
    private Connection con = null;
    private PreparedStatement ps = null;
    private ResultSet rs = null;
    private Disciplinas disciplina;
    private Utilizador utilizadorAtual;
    
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        txtNif.textProperty().addListener((valorDuracao, valorAntigo, novoValor) -> {
            if (txtNif.getText().length() > 9) {
                txtNif.deleteNextChar();
                Alertas.alertaErro("Erro - Número de Identificação Fiscal", "O Número de Identificação Fiscal só pode conter 09 dígitos");

            }
        });

        txtNomeCompleto.textProperty().addListener((valorCliente, valorAntigo, novoValor) -> {

            if (txtNomeCompleto.getText().length() > 255) {
                txtNomeCompleto.deleteNextChar();
                Alertas.alertaErro("Erro - Número de Caracteres Máximo", "Só é possivel guardar nomes de clientes com um tamanho inferior a 255 caracteres");
            }

        });

        txtContactoTelefonico.textProperty().addListener((valorNumVagas, valorAntigo, novoValor) -> {
            if (txtContactoTelefonico.getText().length() > 9) {
                txtContactoTelefonico.deleteNextChar();
                Alertas.alertaErro("Erro - Número de Telefone inválido", "O Número de Telefone só pode conter 09 dígitos");

            }
        });
        
        txtCodPostal.textProperty().addListener((valorNumVagas, valorAntigo, novoValor) -> {
            for(int i = 0; i < txtCodPostal.getText().length(); i++){
                char ch = txtCodPostal.getText().charAt(i);
                if(ch == '-'){
                    //deixa introduzir os dois caracteres em cima;
                }else if (!Character.isDigit(ch)){
                    txtCodPostal.deleteNextChar();
                }
            }
            
            int num = 0;
            
            for(int i = 0; i < txtCodPostal.getText().length();i++){
                if(txtCodPostal.getText().charAt(i) == '-'){
                    num = num + 1;
                }
                if (num > 1){
                    Alertas.alertaErro("Erro", "Código Postal incorreto, insira novamente");
                    txtCodPostal.deleteNextChar();
                }
            }
            
            
        });
        
        
        
        mostrarDisciplinas();
        verDadosDisciplinas();
    }
    
    public void dadosInicio(Utilizador utilizador) {
        utilizadorAtual = utilizador;
    }

    @FXML
    public void criarFormador(ActionEvent e) {
        if (txtNif.getText().isEmpty() == true || txtNomeCompleto.getText().isEmpty() == true || txtRua.getText().isEmpty() == true || txtCodPostal.getText().isEmpty() == true || txtContactoTelefonico.getText().isEmpty() == true) {
            Alertas.alertaErro("Erro - Campos vazios", "Todos os campos têm que ser preenchidos para fazer um registo");
            //break updateBaseDeDados;
        } else {
            try {

                String queryBD = "INSERT INTO Formador(NIF,nome_completo,rua,cod_postal,numero_telefone)"
                                          + " VALUES(" + parseInt(txtNif.getText()) + ",'" + txtNomeCompleto.getText() + "','" + txtRua.getText() + "','" + txtCodPostal.getText() + "'," + parseInt(txtContactoTelefonico.getText()) + " )";
                Statement query = Conexao.connectarBD().createStatement();
                query.executeUpdate(queryBD);
                
                ObservableList<Disciplinas> disciplinasLecionadas = tblViewDisciplina.getSelectionModel().getSelectedItems();
                
                for(int i = 0; i < disciplinasLecionadas.size(); i++){
                    queryBD = "INSERT INTO Formador_Disciplina VALUES (" + parseInt(txtNif.getText()) + "," + disciplinasLecionadas.get(i).getCodDisciplina() + ")" ;
                    query.executeUpdate(queryBD);
                }
                
                Alertas.alertaConfirmacao("Dados inseridos", "Os dados relativamente ao formador foram inseridos na base de dados");
            } catch (SQLException exception) {
                System.out.println(exception.getMessage());
            }

        }
    }
    
    public void mostrarDisciplinas(){
        colunaCodigo.setCellValueFactory(new PropertyValueFactory<>("codDisciplina"));
        colunaNome.setCellValueFactory(new PropertyValueFactory<>("nomeDisciplina"));
    }
    
    public void verDadosDisciplinas() {
        List<Disciplinas> listaDisciplinas = new ArrayList();    
        try{
                
                Statement query = Conexao.connectarBD().createStatement();
                ResultSet resultados = query.executeQuery("SELECT * FROM Disciplina");// SELECT á table view criado para os quartos sem qualquer reserva
                
                while(resultados.next()){
                    Disciplinas disciplina = new Disciplinas(resultados.getInt("cod_disciplina"),resultados.getString("nome"));
                    listaDisciplinas.add(disciplina);//inserir os dados da View na lista observável
                }
        }catch(SQLException e){
                System.out.println(e.getMessage());
            }
        
            ObservableList<Disciplinas> listaDisciplinasObservable = FXCollections.observableArrayList(listaDisciplinas);    
            tblViewDisciplina.setItems(listaDisciplinasObservable); //inserir na TableView todos os dados recolhidos da base de dados
            
            tblViewDisciplina.getSelectionModel().setSelectionMode(
            SelectionMode.MULTIPLE //permitir seleção de múltiplas linhas na TableView.
            );
        }
    
    static String editDiscip;
    
    @FXML
    public void selecionarDisciplina() throws SQLException {
        try {
            Disciplinas disciplina = (Disciplinas) tblViewDisciplina.getSelectionModel().getSelectedItem();
            String query = "SELECT * FROM Disciplina";
            ps = con.prepareStatement(query);

            //editDiscip = disciplina.getNome();
            //txtDisciplina(disciplina.getNome());
            
            ps.close();
            rs.close();
        } catch (SQLException e) {
            System.out.println(e);
        }
    }
    

    @FXML

    public void goToAdministracao(ActionEvent e) throws IOException {

        if (Alertas.alertaConfirmacao("Confirmação", "Deseja voltar para a zona de Administração? Se escolher OK, todos os dados que não foram gravados vão ser apagados").getResult() == ButtonType.OK) {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("/fxml/Administracao.fxml"));
            Parent root = loader.load();
            
            AdministracaoController controller = loader.getController();
            controller.initData(utilizadorAtual); //passar o objeto Utilizador para a próxima Scene;

            Scene scene = new Scene(root);
            scene.getStylesheets().add("/styles/Styles.css");

            Stage stage = (Stage) ((Node) e.getSource()).getScene().getWindow();
            stage.setTitle("Sistema de Gestão de Hoteis");
            stage.setScene(scene);
            stage.show();
        }
    }

    @FXML
    public void apagarDados(ActionEvent e) {

        if (Alertas.alertaConfirmacao("Confirmação - Apagar Dados", "Clique em OK para apagar todos os dados").getResult() == ButtonType.OK) {

            txtNif.setText("");
            txtNomeCompleto.setText("");
            txtRua.setText("");
            txtCodPostal.setText("");
            txtContactoTelefonico.setText("");

        }
    }

    @FXML
    private void integridadeCampoLirico1(javafx.scene.input.KeyEvent event) {
        String caracterEscrito = event.getCharacter();
        for (int i = 0; i < caracterEscrito.length(); i++) {
            Character c = caracterEscrito.charAt(i);

            if (Character.isDigit(c) || !Character.isLetter(c)) { //se o caracter introduzido for um número ou não for uma letra, este será "consumido"
                if (Character.isSpaceChar(c)) {
                    /*
                    o primeiro ciclo testa se o caracter introduzido é um digito e se não é uma letra, ou seja, testa
                    se é um cáracter tipo: @ - , . / ,etc. e também testa se é um espaço em branco, no entanto, se for
                    um espaço branco, têm que se abrir uma exceção pois o campo Nome , recebe o nome completo do cliente,
                    o que indica que vai ser preciso espaços. Para isso, criamos um segundo ciclo if, para estar se é um
                    espaço, se for um espaço, abre uma exceção e deixa o funcionário introduzir, senão, passa para o ciclo
                    else em baixo e "consome" todos as "não-letras"
                     */
                } else {
                    event.consume();
                }
            }
        }
    }

    /**
     * Evento do tipo KeyEvent para garantir que nos campos numéricos, só são
     * introduzidos números e que o número 0 não é introduzido. Todos as letras
     * que o utilizador tente escrever são "consumidas", ou seja, não aparecem
     * no ecrã e são descartadas
     */
    @FXML
    public void integridadeCampoNumerico(javafx.scene.input.KeyEvent event
    ) {

        String caracterEscrito = event.getCharacter();

        for (int i = 0; i < caracterEscrito.length(); i++) {

            Character c = caracterEscrito.charAt(i);

            if (txtContactoTelefonico.textProperty().isEmpty().getValue() == false) { // se a textField não estiver vazia, é permitido introduzir o valor 0, para números como: 10,20,etc.

                if (!Character.isDigit(c)) {
                    event.consume();
                }

            } else { //se a textField estiver vazia, não é permitido introduzir o valor 0, pois o minimo número de pessoas é 1

                if (!Character.isDigit(c) || c == '0') {
                    event.consume();
                }
            }
        }
    }

    /**
     * Evento do tipo KeyEvent para garantir que nos campos numéricos, só são
     * introduzidos números e que o número 0 não é introduzido. Todos as letras
     * que o utilizador tente escrever são "consumidas", ou seja, não aparecem
     * no ecrã e são descartadas
     */
    @FXML
    public void integridadeCampoNIF(javafx.scene.input.KeyEvent event
    ) {

        String caracterEscrito = event.getCharacter();

        for (int i = 0; i < caracterEscrito.length(); i++) {

            Character c = caracterEscrito.charAt(i);

            if (txtNif.textProperty().isEmpty().getValue() == false) { // se a textField não estiver vazia, é permitido introduzir o valor 0, para números como: 10,20,etc.

                if (!Character.isDigit(c)) {
                    event.consume();
                }

            } else {

                if (!Character.isDigit(c) || c == '0') {
                    event.consume();
                }
            }
        }
    }

}
